<?php
namespace Helte\StartUp;
spl_autoload_register(function($class_name){
    if(strpos($class_name, __NAMESPACE__) !== 0) return;
    $rel_path = strtr(substr($class_name, strlen(__NAMESPACE__)+1), '\\', DIRECTORY_SEPARATOR);
    $file = implode(DIRECTORY_SEPARATOR, [
        __DIR__, 'src', $rel_path
    ]) . '.php';
    if(is_readable($file)) require_once $file;
});
