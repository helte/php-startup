# Most generic scripts for Helte
This repository is for Helte to develop more quickly under the order
to fix / update the usability of the services.

## Installation
This repository is expected to use as a library to include in your
PHP application with `include_once` or `require_once`.

Here is `bootstrap.php` to include at once.
