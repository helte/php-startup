<?php
namespace Helte\StartUp;

class TextTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider provideEqualPares
     */
    public function testEq_equal($raw, $str)
    {
        $obj = new Text($raw);
        $this->assertTrue($obj->eq($str), sprintf('"%s" is not equivalent to "%s"', $raw, $str));
    }
    /**
     * @dataProvider provideDifferentPares
     */
    public function testEq_notEqual($raw, $str)
    {
        $obj = new Text($raw);
        $this->assertFalse($obj->eq($str), sprintf('"%s" is equivalent to "%s"', $raw, $str));
    }
    /**
     * @dataProvider provideDifferentPares
     * @depends provideEqualPares
     * @depends testEq_notEqual
     */
    public function testOverwrite_change($raw, $str)
    {
        $obj = new Text($raw);
        $this->assertTrue($obj->overwrite($str)->eq($str), sprintf('"%s" still leaves; it must be "%s"', $raw, $str));
    }
    
    // TODO
    
    public function provideEqualPares()
    {
        return [
            [ 'test', 'test' ],
            [ 'test1', 'test1' ],
            [ '*', '*' ],
            [ 'test', new Text('test') ],
            [ 'test1', new Text('test1') ],
            [ '*', new Text('*') ],
            [ new Text('test'), 'test' ],
            [ new Text('test1'), 'test1' ],
            [ new Text('*'), '*' ]
        ];
    }
    public function provideDifferentPares()
    {
        return [
            [ 'test', '1test' ],
            [ 'test1', 'test2' ],
            [ '*', '**' ],
            [ 'test', new Text('taest') ],
            [ 'test1', new Text('txest1') ],
            [ '*', new Text('**') ],
            [ new Text('0test'), 'test' ],
            [ new Text('1test1'), 'test1' ],
            [ new Text('*'), '**' ]
        ];
    }
}
