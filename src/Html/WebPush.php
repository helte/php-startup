<?php
namespace Helte\StartUp\HTML;

/**
 * Send a push alert to a user's browser
 *
 * This class is designed to work on CRON, not web.
 */
class WebPush
{
    /** @var  string */
    private static $api_url;
    /** @var  string */
    private static $api_key;

    /**
     * Send a browser to push
     *
     * @param array $data
     * @return bool
     */
    final public static function push(array $data)
    {
        $header = [
            'X-BPUSH-TOKEN = '.self::$api_key
        ];
        $options = [
            'http'=>[
                'method'=>'POST',
                'content'=>http_build_query($data),
                'header'=>implode("\r\n", $header)
            ]
        ];
        $response = file_get_contents(self::$api_url, false, stream_context_create($options));
        $result = json_decode($response, true);
        return isset($result['status']) ? $result['status'] === 'success' : false;
    }
}
