<?php
namespace Helte\StartUp\Html;


abstract class Html
{
    /**
     * Get the HTML string for the page title
     *
     * @param string $title Title for the page
     * @return string
     */
    final public static function title($title)
    {
        return '<title>' . $title . '</title>';
    }
    
    /**
     * Get the HTML string for stylesheet
     *
     * @param string $uri URI for the stylesheet file
     * @return string
     */
    final public static function css($uri)
    {
        return '<link rel="stylesheet" href="' . $uri . '" />';
    }

    /**
     * Get the HTML string for JavaScript
     *
     * @param string $uri URI for the JavaScript file
     * @return string
     */
    final public static function script($uri)
    {
        return '<script type="text/javascript" src="'.$uri.'"></script>';
    }
}
