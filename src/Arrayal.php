<?php
namespace Helte\StartUp;


abstract class Arrayal extends Objective implements \Countable
{
    /**
     * @var array
     */
    protected $arr;
    /**
     * @var int
     */
    private $count;

    /**
     * @param array $arr
     */
    public function __construct(array $arr=[])
    {
        $this->__initialize($arr);
    }

    /**
     * Check if the elements in the map are the same as in the given array
     *
     * @param array|Arrayal $another
     * @return bool
     */
    public function eq($another)
    {
        if(is_object($another) && is_a($another, get_class($this))){
            return $this->arr === $another->arr;
        }else if(is_array($another)){
            return $this->arr === $another;
        }
        return false;
    }

    /**
     * Check if the given key(s) has/haves in this
     *
     * @param mixed[] $key1... Variable length arguments
     * @return bool
     */
    public function has($key1)
    {
        $keys = func_get_args();
        foreach($keys as $key){
            if(!isset($this->arr[$key])) return false;
        }
        return true;
    }

    /**
     * Check if the given value(s) exist(s) in this
     *
     * @param mixed $value...  Variable length arguments
     * @return bool
     */
    public function exists($value)
    {
        $values = func_get_args();
        foreach($values as $value){
            if(!in_array($value, $this->arr)) return false;
        }
        return true;
    }

    /**
     * Get the map value or default value
     *
     * @param string|int|float|double|array|Arrayal $key   Scalar for just a key, array or Arrayal for sequential keys
     * @param mixed                                 $value
     * @return $this
     */
    public function set($key, $value)
    {
        if(is_array($key)){
            self::array_set_keys($this->arr, $key, $value);
        }else if(is_object($key) && $key instanceof self){
            self::array_set_keys($this->arr, $key->toArray(), $value);
        }else{
            $this->arr[$key] = $value;
        }
        $this->count = -1;
        return $this;
    }

    /**
     * Get an element or default value
     *
     * @param mixed|mixed[] $key_or_keys
     * @param mixed|null    $default_value Value in case that the key does not exist in the stack
     * @return mixed
     */
    public function get($key_or_keys, $default_value=null)
    {
        if(!is_array($key_or_keys)) $key_or_keys = [$key_or_keys];
        $arr =& $this->arr;
        foreach($key_or_keys as $_key){
            if(!is_array($arr) || !isset($arr[$_key])) return $default_value;
            $arr =& $arr[$_key];
        }
        return $arr;
    }

    /**
     * Walk through each element in this
     *
     * @param callable $mapper
     * @param callable $after  Callback if you want to get this work recursive
     * @return $this
     */
    public function map(callable $mapper, callable $after=null)
    {
        $cls = get_class($this);
        return new $cls(array_map(function($value)use($mapper, $after){
            if($after && is_object($value) && $value instanceof Arrayal){
                $result = $value->map($mapper, $after);
                call_user_func($after, $value);
            }else{
                $result = call_user_func($mapper, $value);
            }
            return $result;
        }, $this->arr));
    }

    /**
     * Drop the array key
     *
     * @param mixed $key
     * @return $this
     */
    public function dropKey($key)
    {
        if(isset($this->arr[$key])){
            unset($this->arr[$key]);
            $this->count--;
        }
        return $this;
    }

    /**
     * Get the number of the elements
     *
     * @return int
     */
    public function count()
    {
        if($this->count < 0){
            $this->count = \count($this->arr);
        }
        return $this->count;
    }

    /**
     * Get an array value
     *
     * @param bool|true $recursive
     * @return array
     */
    final public function toArray($recursive=true)
    {
        $elements = [];
        foreach($this->arr as $key=>$element){
            $elements[$key] = self::toGeneric($element, $recursive);
        }
        return $elements;
    }

    /**
     * Do something
     *
     * @param mixed $value
     * @param bool|true $recursive
     * @return mixed[]
     */
    public static function toGeneric($value, $recursive=true)
    {
        if(is_object($value)){
            return $value instanceof Arrayal ? $value->toArray($recursive) : (string)$value;
        }else if(is_array($value)){
            $arr = [];
            foreach($value as $key=>$v){
                $arr[$key] = self::toGeneric($v);
            }
            return $arr;
        }
        return $value;
    }

    protected function __initialize(array $arr=[])
    {
        $this->arr = $arr;
        $this->count = -1;
    }

    /**
     * Set array value by multiple keys
     *
     * @param array $assoc
     * @param array $keys
     * @param mixed $value
     */
    private static function array_set_keys(array &$assoc, array $keys, &$value)
    {
        $key = array_shift($keys);
        if(!is_scalar($key)) $key = (string)$key;
        if(!isset($keys[0])){
            $assoc[$key] = $value;
        }else{
            if(!isset($assoc[$key]) || !is_array($assoc[$key])) $assoc[$key] = [];
            self::array_set_keys($assoc[$key], $keys, $value);
        }
    }
}
