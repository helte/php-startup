<?php
namespace Helte\StartUp\Switcher;


/**
 * Class to switch a configuration file between branches and
 * in public to access each value
 *
 * @static
 * @final
 */
class Switcher
{
    /**
     * Self object to provide singleton functions
     *
     * @var Switcher
     */
    private static $inst;

    /**
     * @var string
     */
    private $file;

    /**
     * @var array
     */
    private $config;

    /**
     * @param string $file
     * @param array $config
     */
    final private function __construct($file, array $config=[])
    {
        $this->file = $file;
        $this->config = $config;
    }

    /**
     * Load the switch file to start using the configuration
     *
     * @param string $file
     * @return bool
     */
    final public static function load($file)
    {
        if(file_exists($file)){
            $lines = array_map(function($value) {
                return trim($value);
            }, explode("\n", file_get_contents($file)));
            $config = [];
            foreach($lines as $line){
                if(strpos($line, '=') === false) continue;
                list($name, $value) = explode('=', $line, 2);
                $config[$name] = $value;
            }
            self::$inst = new self($file, $config);
            return true;
        }
        return false;
    }

    /**
     * Get the value for the given name; otherwise it returns the default value
     *
     * @param string $name
     * @param mixed|null $default_value
     * @return mixed|null
     */
    final public static function get($name, $default_value=null)
    {
        return isset(self::$inst->config[$name]) ? self::$inst->config[$name] : $default_value;
    }

    /**
     * Set new value for the name
     *
     * @param string $name
     * @param mixed $value
     */
    final public static function set($name, $value)
    {
        self::$inst->config[$name] = $value;
    }

    final public static function apply()
    {
        if($fp = fopen(self::$inst->file, 'w')){
            foreach(self::$inst->config as $name=>$value){
                fwrite($fp, $name.'='.((string)$value));
                fwrite($fp, "\n");
            }
            return fclose($fp);
        }
    }
}
