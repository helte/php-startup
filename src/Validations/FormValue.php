<?php
namespace Helte\StartUp\Validations;


class FormValue
{
    private $user_value;
    private $value;
    private $error;
    private $message;

    public function __construct($user_value, $value, $error=0, $message='ok')
    {
        $this->user_value = $user_value;
        $this->value = $value;
        $this->error = $error;
        $this->message = $message;
    }

    public function getUserValue(){ return $this->user_value; }
    public function getValue(){ return $this->value; }
    public function getError(){ return $this->error; }
    public function getMessage(){ return $this->message; }
    public function okay(){ return $this->error === 0; }
    public function error(){ return $this->error !== 0; }
}

