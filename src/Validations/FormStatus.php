<?php
namespace Helte\StartUp\Validations;

/**
 * Collection interface of FormValue objects
 */
class FormStatus
{
    /**
     * Associative array of FormValue objects
     * 
     * @var FormValue[]
     */
    private $values;
    
    public function __construct()
    {
        $this->values = [];
    }

    /**
     * Set the FormValue object associated with the key
     *
     * @param string    $key
     * @param FormValue $fv
     * @return $this
     */
    public function set($key, FormValue $fv)
    {
        $this->values[$key] = $fv;
        return $this;
    }

    /**
     * Remove the FormValue object from the status
     *
     * @param string $key
     * @return $this
     */
    public function remove($key)
    {
        if(isset($this->values[$key])) unset($this->values[$key]);
        return $this;
    }

    /**
     * Get the FormValue object associated with the key
     *
     * @param string $key
     * @return FormValue
     */
    public function get($key)
    {
        return $this->values[$key];
    }
}
