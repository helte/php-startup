<?php
namespace Helte\StartUp\Validations;

/**
 * Represents the form restriction and validation actually
 */
class Form
{
    private $requires;
    private $optionals;
    private $helpers;
    private $auto_run;

    const AUTO_RUN = true;
    const NOT_AUTO_RUN = false;

    private static $internal_functions;

    public function __construct($auto_run=self::AUTO_RUN)
    {
        $this->requires = [];
        $this->optionals = [];
        $this->helpers = [];
        $this->auto_run = $auto_run;
    }

    /**
     * Set required keys for the user data
     *
     * @param string|string[] $keys...
     * @return $this
     */
    public function requires($keys)
    {
        $args = func_get_args();
        do{
            $arg = array_shift($args);
            if(is_array($arg)){
                foreach($arg as $_a) $args[] = $_a;
            }else{
                $this->requires[] = (string)$arg;
            }
        }while(!empty($args));
        return $this;
    }

    /**
     * Set optional keys for the user data
     *
     * @param string|string[] $keys...
     * @return $this
     */
    public function optionals($keys)
    {
        $args = func_get_args();
        do{
            $arg = array_shift($args);
            if(is_array($arg)){
                foreach($arg as $_a) $args[] = $_a;
            }else{
                $this->optionals[] = (string)$arg;
            }
        }while(!empty($args));
        return $this;
    }

    /**
     * Set the validator onto the key
     *
     * @param string   $key
     * @param callable $validator
     * @param array    $args
     * @return $this
     */
    public function validate($key, callable $validator, array $args=[])
    {
        if(!isset($this->helpers[$key])) $this->helpers[$key] = [];
        $this->helpers[$key][] = [__METHOD__, $validator, $args];
        return $this;
    }

    /**
     * Set the translator onto the key
     *
     * @param string   $key
     * @param callable $translator
     * @param array    $args
     * @return $this
     */
    public function translate($key, callable $translator, array $args=[])
    {
        if(!isset($this->helpers[$key])) $this->helpers[$key] = [];
        $this->helpers[$key][] = [__METHOD__, $translator, $args];
        return $this;
    }

    /**
     * Run the required key check and validators, and also translators
     *
     * @param array $data Associative array as the user data
     * @return FormStatus Always return FormStatus object;
     *                    to confirm if it's success, run FormStatus::error() / FormStatus::success()
     */
    public function apply(array $data)
    {
        $status = new FormStatus();
        foreach($data as $_key=>$_value){
            $status->set($_key, new FormValue($_value, $_value));
        }
        if(!is_array(self::$internal_functions)) self::$internal_functions = get_defined_functions()['internal'];
        foreach([
                    'check_required_keys',
                    'drop_unnecessary_keys',
                    'run_helpers'
                ] as $_fn){
            if(!$this->$_fn($data, $status)) return $status;
        }
        return $status;
    }

    /**
     * Check if there are all required keys in the user data
     *
     * @param array $data        Associative array as the user data
     * @param FormStatus $status Current FormStatus associated with the user data
     * @return bool              False when there is at least one missing key
     */
    private function check_required_keys(array $data, FormStatus $status)
    {
        $missings = array_diff($this->requires, array_keys($data));
        foreach($missings as $_key){
            $status->set($_key, new FormValue(null, null, 404, $_key.' is required'));
        }
        return empty($missings);
    }

    /**
     * Drop all unnecessary keys to avoid unexpected troubles
     *
     * @param array $data        Associative array as the user data
     * @param FormStatus $status Current FormStatus associated with the user data
     * @return $this             Always returns positive result by the object
     */
    private function drop_unnecessary_keys(array $data, FormStatus $status)
    {
        $unnecessary = array_diff(array_keys($data), array_merge($this->requires, $this->optionals));
        foreach($unnecessary as $_key) $status->remove($_key);
        return $this;
    }

    /**
     * Run the validators/translators on the values
     *
     * @param array $data        Associative array as the user data
     * @param FormStatus $status Current FormStatus associated with the user data
     * @return bool              False when at least one of the validation has failed
     */
    private function run_helpers(array $data, FormStatus $status)
    {
        $stops = [];
        foreach($this->helpers as $_key=>$_helpers){
            foreach($_helpers as $_helper){
                if(!isset($data[$_key]) || isset($stops[$_key])) continue;
                if(in_array($_helper[1], self::$internal_functions)){
                    $r = call_user_func($_helper[1], $status->get($_key)->getValue());
                }else{
                    $r = call_user_func_array($_helper[1], array_merge([
                        $status->get($_key)->getValue(), $_key, $this, $status
                    ], $_helper[2]));
                }
                switch($_helper[0]){
                    case 'validate':
                        if($r === false){
                            $status->set($_key, new FormValue($data[$_key], $data[$_key], 400, 'wrong type'));
                            $stops[$_key] = true;
                        }
                        break;
                    case 'translate':
                        $status->set($_key, new FormValue($status->get($_key)->getValue(), $r));
                        break;
                }
            }
        }
        return empty($stops);
    }
}
