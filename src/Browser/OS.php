<?php
namespace Helte\StartUp\Browser;


abstract class OS
{
    const UNKNOWN       = 0;
    const WINDOWS       = 1;
    const MACINTOSH     = 2;
    const LINUX         = 3;
    const ANDROID       = 10;
    const IOS           = 11;
    const WINDOWS_PHONE = 12;
    const BLACKBERRY_OS = 13;
    const FIREFOX_OS    = 14;
    const CHROME_OS     = 15;
    const KINDLE_OS     = 100;
}