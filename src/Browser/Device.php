<?php
namespace Helte\StartUp\Browser;


abstract class Device
{
    const OTHER        = 0;
    const PC           = 10;
    const PC_WINDOWS   = 11;
    const PC_MACINTOSH = 12;
    const PC_LINUX     = 13;
    const MOBILE       = 100;
    const SMART_PHONE  = 110;
    const SP_ANDROID   = 111;
    const SP_IOS       = 112;
    const SP_WINDOWS   = 113;
    const SP_FIREFOX   = 114;
    const SP_BLACKBERRY = 115;
    const TABLET       = 120;
    const TB_ANDROID   = 121;
    const TB_IOS       = 122;
    const TB_WINDOWS   = 123;
    const TB_FIREFOX   = 124;
    const TB_BLACKBERRY = 125;
    const TB_KINDLE    = 126;
}