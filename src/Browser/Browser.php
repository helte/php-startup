<?php
namespace Helte\StartUp\Browser;


abstract class Browser
{
    const OTHER        = 0;
    const CRHOME       = 1;
    const OPERA        = 2;
    const IE           = 3;
    const FIREFOX      = 4;
    const SAFARI       = 5;
    const SMART_PHONE  = 110;
    const SP_ANDROID   = 111;
    const SP_IOS       = 112;
    const SP_WINDOWS   = 113;
    const SP_FIREFOX   = 114;
    const SP_BLACKBERRY = 115;
    const TABLET       = 120;
    const TB_ANDROID   = 121;
    const TB_IOS       = 122;
    const TB_WINDOWS   = 123;
    const TB_FIREFOX   = 124;
    const TB_BLACKBERRY = 125;
    const TB_KINDLE    = 126;
}