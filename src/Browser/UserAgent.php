<?php
namespace Helte\StartUp\Browser;


class UserAgent
{
    /**
     * Raw string of User Agent
     *
     * @var string
     */
    private $ua;

    /**
     * Device type
     *
     * @var int
     * @see \OhtaMasato\Browser\Device
     */
    private $device;

    /**
     * OS type
     *
     * @var int
     * @see \OhtaMasato\Browser\OS
     */
    private $os;

    /**
     * Browser type
     *
     * @var int
     * @see \OhtaMasato\Browser\Browser
     */
    private $browser;

    /**
     * @param null|string $ua Raw string of User Agent; if nothing is given, it automatically
     *                        uses HTTP_USER_AGENT
     */
    public function __construct($ua=null)
    {
        $this->setUserAgent($ua);
    }

    /**
     * Get the array of browser information
     *
     * @return array
     */
    final public function toArray()
    {
        return [
            'device'=>$this->device,
            'browser'=>$this->browser,
            'os'=>$this->os
        ];
    }

    /**
     * Get the raw string of User Agent
     *
     * @return string
     */
    final public function raw()
    {
        return $this->ua;
    }

    /**
     * Get the device type
     *
     * @return int
     * @see \OhtaMasato\Browser\Device
     */
    final public function device()
    {
        return $this->device;
    }

    /**
     * Get the OS type
     *
     * @return int
     * @see \OhtaMasato\Browser\OS
     */
    final public function os()
    {
        return $this->os;
    }

    /**
     * Get the browser type
     *
     * @return int
     * @see \OhtaMasato\Browser\Browser
     */
    final public function browser()
    {
        return $this->browser;
    }

    /**
     * Set a new User Agent
     *
     * @param string $ua Raw string of User Agent
     * @return $this
     */
    public function setUserAgent($ua)
    {
        if(!is_string($ua)) $ua = $_SERVER['HTTP_USER_AGENT'];
        $this->ua = mb_strtolower($ua);

        if(strpos($this->ua,'iphone') !== false){
            $this->device = Device::SMART_PHONE;
            $this->os = OS::IOS;
        }elseif(strpos($this->ua,'ipod') !== false){
            $this->device = Device::SMART_PHONE;
            $this->os = OS::IOS;
        }elseif((strpos($this->ua,'android') !== false) && (strpos($this->ua, 'mobile') !== false)){
            $this->device = Device::SMART_PHONE;
            $this->os = OS::ANDROID;
        }elseif((strpos($this->ua,'windows') !== false) && (strpos($this->ua, 'phone') !== false)){
            $this->device = Device::SMART_PHONE;
            $this->os = OS::WINDOWS_PHONE;
        }elseif((strpos($this->ua,'firefox') !== false) && (strpos($this->ua, 'mobile') !== false)){
            $this->device = Device::SMART_PHONE;
            $this->os = OS::FIREFOX_OS;
        }elseif(strpos($this->ua,'blackberry') !== false){
            $this->device = Device::SMART_PHONE;
            $this->os = OS::BLACKBERRY_OS;
        }elseif(strpos($this->ua,'ipad') !== false){
            $this->device = Device::TABLET;
            $this->os = OS::IOS;
        }elseif((strpos($this->ua,'windows') !== false) && (strpos($this->ua, 'touch') !== false && (strpos($this->ua, 'tablet pc') == false))){
            $this->device = Device::TABLET;
            $this->os = OS::WINDOWS;
        }elseif((strpos($this->ua,'android') !== false) && (strpos($this->ua, 'mobile') === false)){
            $this->device = Device::TABLET;
            $this->os = OS::ANDROID;
        }elseif((strpos($this->ua,'firefox') !== false) && (strpos($this->ua, 'tablet') !== false)){
            $this->device = Device::TABLET;
            $this->os = OS::FIREFOX_OS;
        }elseif((strpos($this->ua,'kindle') !== false) || (strpos($this->ua, 'silk') !== false)){
            $this->device = Device::TABLET;
            $this->os = OS::KINDLE_OS;
        }elseif((strpos($this->ua,'playbook') !== false)){
            $this->device = Device::TABLET;
            $this->os = OS::BLACKBERRY_OS;
        }else{
            $this->device = Device::OTHER;
            $this->os = OS::UNKNOWN;
        }

        $this->getBrowser();

        return $this;
    }

    /**
     * Update the browser type as the current user agent
     *
     * @return $this
     */
    final protected function getBrowser()
    {
        if(strpos($this->ua,'chrome') !== false){
            $this->browser = Browser::CRHOME;
        }elseif(strpos($this->ua,'opera') !== false){
            $this->browser = Browser::OPERA;
        }elseif(strpos($this->ua,'msie') !== false){
            $this->browser = Browser::IE;
        }elseif(strpos($this->ua,'firefox') !== false){
            $this->browser = Browser::FIREFOX;
        }elseif(strpos($this->ua,'safari') !== false){
            $this->browser = Browser::SAFARI;
        }else{
            $this->browser = Browser::OTHER;
        }
        return $this;
    }
}
