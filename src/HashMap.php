<?php
namespace Helte\StartUp;

/**
 * Class available to chain methods
 */
class HashMap extends \ArrayObject
{
    /**
     * Applies the callback to the elements of the given arrays
     *
     * @param callable $callback Callback function to run for each element in each array.
     * @return $this
     */
    public function map(callable $callback)
    {
        $data = $this->getArrayCopy();
        foreach($data as $key=>&$value){
            $value = call_user_func($callback, $value, $key);
        }
        return $this->spawn($data);
    }

    /**
     * Filters elements of an array using a callback function
     *
     * @param callable $callback The callback function to use
     * @param int      $flag     Flag determining what arguments are sent to callback:
     *                           - ARRAY_FILTER_USE_KEY - pass key as the only argument to callback instead of the value
     *                           - ARRAY_FILTER_USE_BOTH - pass both value and key as arguments to callback instead of the value
     * @return $this
     */
    public function filter(callable $callback, $flag=\ARRAY_FILTER_USE_BOTH)
    {
        return $this->spawn(\array_filter($this->getArrayCopy(), $callback, $flag));
    }

    /**
     * Modify keys associated with values in the array
     *
     * @param callable $callback The callback function to use
     *                           arguments: function(scalar $key, mixed $value)
     * @return $this
     */
    public function modify(callable $callback)
    {
        $data = [];
        foreach($this->getArrayCopy() as $key=>$value){
            $key = call_user_func($callback, $key, $value);
            $data[$key] = $value;
        }
        return $this->spawn($data);
    }

    /**
     * Get the pure array with the given keys and
     * other keys will be all dropped
     *
     * @return $this
     */
    public function pure(array $keys)
    {
        return $this->filter(function($key)use($keys){
            return in_array($key, $keys);
        });
    }

    /**
     * Sort by values with a comparator
     *
     * @param callable $comparator
     * @return $this
     */
    public function sort(callable $comparator)
    {
        $a = clone $this;
        $a->uasort($comparator);
        return $a;
    }

    /**
     * Return all the keys or a subset of the keys of an array
     *
     * @return $this
     */
    public function keys()
    {
        return $this->spawn(\array_keys($this->getArrayCopy()));
    }

    /**
     * Return all the values of an array
     *
     * @return $this
     */
    public function values()
    {
        return $this->spawn(\array_values($this->getArrayCopy()));
    }

    /**
     * Merge all arrays
     *
     * NOTE: it overwrites later scalar elements if the primer has.
     * Array elements are recursively merged instead.
     *
     * @param array $b...
     * @return array
     */
    public function merge(array $b){
        $arrays = func_get_args();
        $a = $this->getArrayCopy();
        foreach($arrays as $b){
            if(!is_array($b)) continue;
            foreach($b as $i=>$_e){
                if(is_string($i)){
                    if(isset($a[$i])){
                        if(is_array($a[$i]) && is_array($_e)) {
                            $a[$i] = self::merge($a[$i], $_e);
                        }else{
                            $a[$i] = $_e;
                        }
                    }else{
                        $a[$i] = $_e;
                    }
                }else if(!in_array($_e, $a)){
                    $a[] = $_e;
                }
            }
        }
        return $this->spawn($a);
    }

    /**
     * Instantiate another object of the class
     *
     * @param array $data
     * @return $this
     */
    public function spawn(array $data=[])
    {
        $cls = get_class($this);
        return new $cls($data);
    }
}
