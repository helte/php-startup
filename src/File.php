<?php
namespace Helte\StartUp;
use Helte\StartUp\Concepts\Real;

/**
 * Local disk object as a file
 */
class File extends DiskObject
{
    /**
     * Get the file extension
     *
     * @param bool|false $include_ext True to include the extension
     * @return string
     */
    final public function getBaseName($include_ext=false){
        $a = explode('/', strtr($this->getPath(), '\\', '/'));
        $base_name = array_pop($a);
        if(!$include_ext){
            $i = strrpos($base_name, '.');
            if(is_int($i)) $base_name = substr($base_name, 0, $i+1);
        }
        return $base_name;
    }
    
    /**
     * Set the file extension
     *
     * @param string $base_name       Base name to replace
     * @param bool|false $include_ext True to exclude the current extension
     * @return $this
     */
    final public function setBaseName($base_name, $include_ext=false){
        if(!$include_ext){
            $base_name .= $this->getExtension();
        }
        $a = explode('/', strtr($this->getPath(), '\\', '/'));
        array_pop($a);
        $a[] = $base_name;
        return $this->spawn(implode('/', $a));
    }
    
    /**
     * Get the file extension
     *
     * @param bool|false $exclude_dot True to exclude "." in front of the extension letters
     * @return string
     */
    final public function getExtension($exclude_dot=false){
        $i = strrpos($this->getPath(), '.');
        return is_int($i) ? substr($this->getPath(), $i+((int)$exclude_dot)) : '';
    }
    
    /**
     * Set the file extension
     *
     * @param bool|false $prepend_dot True to prepend "." for the extension letters
     * @return $this
     */
    final public function setExtension($ext, $prepend_dot=false){
        $i = strrpos($this->getPath(), '.');
        return $this->spawn((is_int($i) ? substr($this->getPath(), 0, $i-1) : '') . ($prepend_dot ? '.' : '') . $ext);
    }

    /**
     * Change the file access permission
     *
     * @param int|string $permission
     * @return boolean
     * @see chmod() permission is used for this PHP-defined function
     */
    public function permit($permission=0777)
    {
        return chmod($this->getPath(), $permission);
    }

    /**
     * Get the size of the disk object
     *
     * @return Real
     */
    public function size(){ return new Real(filesize($this->getPath())); }
}
