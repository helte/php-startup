<?php
namespace Helte\StartUp;


abstract class ClassDefinition
{
    /**
     * Get the base name of the class
     * 
     * @param string $class_name
     * @return string
     */
    final public static function getBaseName($class_name)
    {
        if(is_object($class_name)) $class_name = get_class($class_name);
        $digits = explode('\\', $class_name);
        return array_pop($digits);
    }
}
