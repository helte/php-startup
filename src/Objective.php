<?php
namespace Helte\StartUp;

/**
 * Base class to serve general class methods
 */
abstract class Objective
{
    /**
     * Check if the value is the same or not
     *
     * @param Objective $another
     * @return bool
     */
    public function eq($another)
    {
        return $this === $another;
    }
    
    /**
     * Instantiate another object of the class
     *
     * @param mixed... $arg If an argument is given, instantiate it with new declaration exactly.
     *                      If arguments are wrong or missing, PHP rises an error, not from this method.
     * @return $this
     */
    public function spawn()
    {
        $args = func_get_args();
        if(empty($args)) return clone $this;
        $cls = get_class($this);
        $xs = [];
        for($i=0, $k=count($args); $i<$k; $i++) $xs[] = '$args['.$i.']';
        $obj = eval('return new '.$cls.'('.implode(',', $xs).')');
        return $obj;
    }
}
