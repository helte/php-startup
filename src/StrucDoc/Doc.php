<?php
namespace Helte\StartUp\StrucDoc;

use Helte\StartUp\Text;

class Doc
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var Text
     */
    private $name;
    /**
     * @var Text
     */
    private $directory;
    /**
     * @var Text
     */
    private $path;
    /**
     * @var Text
     */
    private $status;
    /**
     * @var Text
     */
    private $body;

    /**
     * Instantiate the class from "path" which starts with "/"
     *
     * @param Text|string $path
     * @return Doc
     */
    public static function fromPath($path)
    {
        $path = new Text($path);
        if(!$path->sub(0, 1)->eq('/')) $path->prepend('/');
        if($path->sub(0, 2)->eq('./')) $path->cut(1);

        $directory = new Text(dirname($path));
        $name = new Text(basename($path));
        $type = new Text($name->has('.') ? 'file' : 'directory');
        if($directory->eq('.')) $directory->overwrite('');
        if($directory->sub(0, 1)->eq('/')) $directory->cut(1);
        return new self(
            $path,
            $directory,
            $name,
            $type
        );
    }

    /**
     * @param array $profile
     *               "path"
     *               "directory"
     *               "name"
     *               "type"
     *               "body"
     *               "status"
     *               "id"
     * @return Doc
     */
    public static function fromArray(array $profile)
    {
        return new self(
            new Text($profile['path']),
            new Text($profile['directory']),
            new Text($profile['name']),
            new Text($profile['type']),
            new Text($profile['body']),
            new Text($profile['status']),
            (int)$profile['id']
        );
    }

    /**
     * @param Text $path
     * @param Text $directory
     * @param Text $name
     * @param Text $type
     * @param Text $body
     * @param Text $status
     * @param int  $id
     */
    public function __construct(Text $path, Text $directory, Text $name, Text $type, Text $body=null, Text $status=null, $id=-1)
    {
        $this->path      = $path;
        $this->directory = $directory;
        $this->name      = $name;
        $this->type      = $type;
        $this->id        = $id;
        $this->body      = is_null($body) ? new Text('') : $body;
        $this->status    = is_null($status) ? new Text('draft') : $status;
    }

    /**
     * @param array $profile
     *               "path"
     *               "directory"
     *               "name"
     *               "type"
     *               "body"
     *               "status"
     *               "id"
     * @return Doc
     */
    public function applyArray(array $profile)
    {
        foreach($profile as $key=>$value){
            $this->$key = $key === 'id' ? (int)$value : new Text($value);
        }
        return $this;
    }

    /**
     * Check if the root directory doc or not
     *
     * @return bool
     */
    public function isRoot(){ return $this->directory->eq('') && $this->name->eq(''); }

    /**
     * Check if it's in the root directory doc or not
     *
     * @return bool
     */
    public function isRootChild(){ return $this->directory->eq(''); }

    /**
     * Get the id
     *
     * @return int
     */
    public function getId(){ return $this->id; }

    /**
     * Get the directory which does NOT start with "/"
     *
     * @return Text
     */
    public function getDirectory(){ return $this->directory; }

    /**
     * Get the base name
     *
     * @return Text|string
     */
    public function getName(){ return $this->name; }

    /**
     * Get the absolute path which starts with "/"
     *
     * @return Text
     */
    public function getPath(){ return $this->path; }

    /**
     * Get the type of the doc
     *
     * @return Text
     */
    public function getType(){ return $this->type; }

    /**
     * Get the document's current status: "draft", "published", "deprecated"
     *
     * @return Text
     */
    public function getStatus(){ return $this->status; }

    /**
     * Get the document body
     *
     * @return Text
     */
    public function getBody(){ return $this->body; }

    /**
     * Get the directory for a child
     *
     * @return Text
     */
    public function getChildDir()
    {
        return $this->path->sub(1);
    }

    /**
     * Get the path for the parent
     *
     * @return Text
     */
    public function getParentPath()
    {
        $dir = clone $this->directory;
        return $dir->prepend('/');
    }

    /**
     * Check if the document is draft
     *
     * @return bool
     */
    public function isDraft(){ return $this->status->eq('draft'); }

    /**
     * Check if the document is public
     *
     * @return bool
     */
    public function isPublic(){ return $this->status->eq('published'); }

    /**
     * Check if the document is deprecated
     *
     * @return bool
     */
    public function isDeprecated(){ return $this->status->eq('deprecated'); }

    /**
     * Get properties in array
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'path'      => (string)$this->path,
            'directory' => (string)$this->directory,
            'name'      => (string)$this->name,
            'type'      => (string)$this->type,
            'id'        => $this->id,
            'body'      => (string)$this->body,
            'status'    => (string)$this->status
        ];
    }
}