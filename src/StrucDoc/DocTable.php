<?php
namespace Helte\StartUp\StrucDoc;

use Helte\StartUp\Map;
use Helte\StartUp\Theme\Theme;

class DocTable
{
    /**
     * @var Theme
     */
    private $theme;

    /**
     * @var \mysqli
     */
    private $db;

    /**
     * @param Theme   $theme
     * @param \mysqli $db
     */
    public function __construct(Theme $theme, \mysqli $db)
    {
        $this->theme = $theme;
        $this->db = $db;
    }

    /**
     * Load the nesr indexes for the sidebar
     *
     * @return Doc[]
     */
    public function getNearIndexes(Doc $doc)
    {
        $child_dir   = $this->db->real_escape_string($doc->getChildDir());
        $brother_dir = $this->db->real_escape_string($doc->getDirectory());
        $parent_path = $this->db->real_escape_string($doc->getParentPath());
        $current_id  = $this->db->real_escape_string($doc->getId());

        $where = '`status`=\'published\'';
        if($doc->isRoot()){
            $where .= sprintf('AND (`directory`= \'%s\' OR `id`=\'%s\')', $child_dir, $current_id);
        }else{
            $where .= sprintf(
                'AND (`directory` = \'%s\' OR `directory` = \'%s\' OR `path`=\'%s\' OR `id`=\'%s\')',
                $child_dir, $brother_dir, $parent_path, $current_id
            );
        }

        $sql = 'SELECT * FROM `strucdoc_contents` WHERE '. $where . 'ORDER BY `path` ASC';
        $this->theme->debug($sql);
        $result = $this->db->query($sql);
        $indexes = [];
        while($row = $result->fetch_assoc()){
            $indexes[] = Doc::fromArray($row);
        }
        return $indexes;
    }

    /**
     * Load the document data
     *
     * @param Doc $doc
     * @return bool|Doc
     */
    public function loadThe(Doc $doc)
    {
        $status_cond = isset($_GET['edit']) ? '' : 'AND `status`=\'published\'';
        $sql = sprintf('SELECT * FROM `strucdoc_contents` WHERE `path`=\'%s\' %s ORDER BY `id` DESC',
            $this->db->real_escape_string($doc->getPath()),
            $status_cond);
        $this->theme->debug($sql);
        $result = $this->db->query($sql);
        while($row = $result->fetch_assoc()){
            return $doc->applyArray($row);
        }
        return false;
    }

    /**
     * Create a new draft of the post
     *
     * @param Doc $doc
     * @return int Positive number for success, negative number for failure
     */
    public function draft(Doc $doc)
    {
        $data1 = array_merge((new Map($doc->toArray()))->filterKeyByArray(['body'])->toArray(), [
            'directory' => $doc->getDirectory(),
            'name' => $doc->getName(),
            'path'=> $doc->getPath(),
            'type' => $doc->getType(),
            'status'=>'draft'
        ]);

        $db = $this->db;
        $keys   = '`'.implode('`, `', array_keys($data1)).'`';
        $values = '\''.implode('\', \'', array_map(function($value)use($db){
                return $db->real_escape_string($value);
            }, array_values($data1))).'\'';

        $sql = sprintf('INSERT INTO `strucdoc_contents` (%s)VALUES(%s)', $keys, $values);
        $this->theme->debug($sql);
        if($this->db->query($sql)){
            return $this->db->insert_id;
        }else{
            return -1;
        }
    }

    /**
     * Search some documents by keywords
     *
     * @param string $user_keyword
     * @return Doc[]
     */
    public function search($user_keyword)
    {
        $rows = [];
        $keywords = explode(' ', $user_keyword);
        $and_conds = [];
        $fields = ['name', 'directory', 'path', 'body'];
        foreach($keywords as $keyword){
            $or_conds = [];
            foreach($fields as $field){
                $or_conds[] = sprintf('`%s` LIKE \'%s\'', $field, '%'.$this->db->real_escape_string($keyword).'%');
            }
            $and_conds[] = sprintf('(%s)', implode(' OR ', $or_conds));
        }
        $where = implode(' AND ', $and_conds);
        $sql = sprintf('SELECT * FROM `strucdoc_contents` WHERE %s ORDER BY `id` DESC',$where);
        $this->theme->debug($sql);
        $result = $this->db->query($sql);
        while($row = $result->fetch_assoc()){
            $rows[] = Doc::fromArray($row);
        }
        return $rows;
    }

    /**
     * Update the document
     *
     * @param Doc $origin
     * @param Map $post
     * @return bool|\mysqli_result
     */
    public function update(Doc $origin, Map $post)
    {
        $keys = ['type', 'body', 'status', 'path']; // Keep "path" at the end
        if($post1 = $post->filterKeyByArray($keys, true)){
            $doc = Doc::fromPath($post1->get(array_pop($keys)));
            $user_data = $post1->filterKeyByArray($keys)->toArray();
            $doc->applyArray($user_data);

            $data = array_merge($user_data, [
                'path'=>$doc->getPath(),
                'directory'=>$doc->getDirectory(),
                'name'=>$doc->getName()
            ]);

            $sets = [];
            foreach($data as $field=>$value){
                $sets[] = sprintf('`%s`=\'%s\'', $field, $this->db->real_escape_string($value));
            }

            $sql = sprintf('UPDATE `strucdoc_contents` SET %s WHERE `path`=\'%s\' ORDER BY id DESC LIMIT 1',
                implode(', ', $sets),
                $this->db->real_escape_string($origin->getPath()));
            $this->theme->debug($sql);
            if($this->db->query($sql)){
                $origin->applyArray($data);
                return $origin;
            }
            return false;
        }else{
            return false;
        }
    }
}
