<?php
namespace Helte\StartUp\System;

/**
 * Exception to throw when the given local code for the previous exception is out of range.
 */
class LocalCodeException extends \Exception
{
    /** @var int Error code for this exception */
    const E_CODE = 33339999;

    /**
     * @param int $direction more than 0 means exceeds, 0 or less than 0 means negative
     * @param \Exception|null $previous
     */
    public function __construct($direction, \Exception $previous=null)
    {
        if($direction > 0){
            $message = 'Given local error code exceeds the maximum';
            $code = self::E_CODE - 1;
        }else{
            $message = 'Negative number for error code: positive number is only acceptable';
            $code = self::E_CODE;
        }
        parent::__construct($message, $code, $previous);
    }
}
