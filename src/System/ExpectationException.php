<?php
namespace Helte\StartUp\System;

/**
 * Exception to throw when something unexpected happens.
 */
class ExpectationException extends \Exception
{
    /** @var int Base error code for this exception */
    const E_CODE = 99990000;

    /**
     * @param string      $message      Message to explain what happens
     * @param int         $local_code   Error code in local; 0 - 9999 to specify the error by the code
     * @param \Exception|null $previous
     * @throws LocalCodeException
     */
    public function __construct($message, $local_code=0, \Exception $previous=null)
    {
        if($local_code < 0) throw new LocalCodeException(-1, $this);
        if($local_code > 9999) throw new LocalCodeException(1, $this);
        parent::__construct($message, self::E_CODE + $local_code, $previous);
    }
}
