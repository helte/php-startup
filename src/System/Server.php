<?php
namespace Helte\StartUp\System;

/**
 * Control of the server on which this PHP process runs
 */
class Server
{
    /**
     * Linux user name associated with /home directory to run as product
     * Otherwise any user would be a test user.
     *
     * @var string
     */
    private static $product_user = 'sites';
    /**
     * Current user for self::$product_user
     *
     * NOTE: this value is just for cache. Don't substitute anything directly.
     *
     * @var string
     */
    private static $current_user;
    /**
     * Cache value for detection if the current process is running as test for the system
     *
     * NOTE: this value is just for cache. Don't substitute anything directly.
     *
     * @var string
     */
    private static $is_test;

    /**
     * Set the product user name
     *
     * NOTE: the user name should be associated with /home directory
     *
     * @param string $user_name
     */
    public static function setProductUser($user_name)
    {
        self::$product_user = $user_name;
    }

    /**
     * Get the product user name
     *
     * @return string
     */
    public static function getProductUser()
    {
        return self::$product_user;
    }

    /**
     * Get the current user name
     *
     * NOTE: this library depends on running with document root in /home sub or descendant directory.
     * Otherwise an exception will be thrown
     *
     * @return string
     * @throws \Exception
     */
    public static function getCurrentUser()
    {
        if(is_null(self::$current_user)){
            if(!isset($_SERVER['HOME']) && !isset($_SERVER['DOCUMENT_ROOT'])) throw new ExpectationException('HOME nor DOCUMENT_ROOT is missing in $_SERVER');
            $digits = explode(DIRECTORY_SEPARATOR, $_SERVER[isset($_SERVER['HOME']) ? 'HOME' : 'DOCUMENT_ROOT']);
            if(!isset($digits[1])) throw new ExpectationException('HOME nor DOCUMENT_ROOT is blank in $_SERVER');
            if($digits[1] !== 'home' || !isset($digits[2])) throw new ServerConfigurationException('Wrong document root: httpd should be running in /home user directory.');
            if($digits[2] === 'sites' && $digits[3] === 'tests'){
                self::$current_user = 'thomasohta';
            }else{
                self::$current_user = $digits[2];
            }
        }
        return self::$current_user;
    }

    /**
     * Check if the library is installed by keywords
     *
     * NOTE: currently it only supports CentOS yum.
     * If you want to check others, please modify this.
     * 
     * @param string[]|string $keyword
     * @return bool
     */
    public static function installed($keyword)
    {
        $keywords = is_array($keyword) ? $keyword : [$keyword];
        /// @example "grep php | grep gd | grep x86_64"
        $conditions = implode(' | ', array_map(function($keyword){
            return 'grep ' . $keyword;
        }, $keywords));
        $output = exec('yum list installed | ' . $conditions);
        if(!is_string($output)) return false;
        $output_lines = explode("\n", $output);
        foreach($output_lines as $_line){
            foreach($keywords as $_keyword){
                if(stripos($_line, $_keyword) === false) return false;
            }
        }
        return true;
    }

    /**
     * Check if the library is NOT installed by keywords
     *
     * @param string[]|string $keyword
     * @return bool True for being absent, false for being installed
     * @see Server::installed()
     */
    public static function absent($keyword)
    {
        return !self::installed($keyword);
    }
    
    /**
     * Check if the current server is local or not
     *
     * @param string|null $host
     * @return bool
     */
    public static function local($host=null)
    {
        $host = self::host($host);
        $server_ip = self::serverIp();
        if($host === 'localhost' || $server_ip === '127.0.0.1') return true;
        if(strpos($server_ip, '192.168.') === 0) return true;
        if(strpos($server_ip, '172.') === 0) return true;
        return false;
    }

    /**
     * Check if the current server is on test or not
     *
     * NOTE: on CLI, with `--test` option, you can force set as test
     * 
     * @param string|null $host
     * @return bool
     */
    public static function test($host=null)
    {
        if(is_null(self::$is_test)){
            if(is_null($host) && self::getProductUser() !== self::getCurrentUser()){
                self::$is_test = true;
            }else{
                $needle = 'test';
                if(php_sapi_name() === 'cli'){
                    global $argv;
                    if(in_array('--test', $argv)) return true;
                    $pos = strpos(__DIR__, $needle);
                }else{
                    $pos = strpos(is_string($host) ? $host : self::value('name', $needle), $needle);
                }
                self::$is_test = is_int($pos) && $pos > 0;
            }
        }
        return self::$is_test;
    }

    /**
     * Force to be a host name
     *
     * @param string|null $given
     * @return string
     */
    public static function host($given=null)
    {
        if(!is_string($given)){
            return self::value('name', 'localhost');
        }
        return $given;
    }

    /**
     * Get the value of the server information
     *
     * @param string $name
     * @param mixed|null $default_value
     * @return mixed
     * @see $_SERVER
     */
    public static function value($name, $default_value=null)
    {
        $name = 'SERVER_' . strtoupper($name);
        return isset($_SERVER[$name]) ? $_SERVER[$name] : $default_value;
    }

    /**
     * Get the current server IP address (possibly local IP)
     *
     * @return string
     */
    private static function serverIp(){ return self::value('addr', 'localhost'); }
}
