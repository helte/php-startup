<?php
namespace Helte\StartUp\System;


class Template
{
    /** @var string Template file directory */
    private static $dir;

    /**
     * @param string $dir Template file directory
     */
    public static function setDir($dir)
    {
        self::$dir = $dir;
    }

    /**
     * @return string Template file directory
     */
    public static function getDir()
    {
        return self::$dir;
    }

    /**
     * Translate the content with translation key-value pare array
     *
     * @param string $content
     * @param array  $translation
     * @return string
     * 
     * NOTE: all hypens will be replaced with underscore, and string will be done into lower case
     */
    public static function translate($content, array $translation=[])
    {
        foreach($translation as $key=>$value) $content = str_replace('<%'.strtr(strtolower($key),'-', '_').'%>', $value, $content);
        return $content;
    }
    
    
    
    /** @var string */
    private $name;
    /** @var string */
    private $timezone;
    /** @var string */
    private $extension;
    /** @var string */
    const DEFAULT_TIMEZONE = 'default/default';
    
    public function __construct($template_name, $timezone=self::DEFAULT_TIMEZONE, $extension='.mt')
    {
        $this->timezone = $timezone;
        $this->name = $template_name;
        $this->extension = $extension;
    }

    /**
     * Get the template content
     *
     * @param string $type
     * @param array  $values key value array
     * @return string|false
     */
    public function getContent($type, array $values=[])
    {
        $content = $this->getRawContent($type);
        if(is_string($content)){
            return self::translate($content, array_merge(
                get_defined_constants(),
                $this->collect_environment_values(),
                $this->collect_date_time(),
                $values));
        }else if($this->timezone !== self::DEFAULT_TIMEZONE){
            $this->timezone = self::DEFAULT_TIMEZONE;
            return $this->getContent($type, $values);
        }
        return false;
    }

    /**
     * Get the template content
     *
     * @param string $type
     * @return string|false
     */
    public function getRawContent($type)
    {
        if(!is_string(self::$dir)){
            new \LogicException('Template::$dir has not been initialized: set the template directory path in string');
        }
        $file = implode('/', [self::$dir, strtolower($this->timezone), $type, $this->name]) . $this->extension;
        return file_get_contents($file);
    }

    /**
     * Collect the system environment values
     *
     * @return array Keys is in string, value is also in string
     */
    private function collect_environment_values()
    {
        $env = [];
        foreach(['server','post','get'] as $n){
            $vn = '_'.strtoupper($n);
            if(isset($GLOBALS[$vn])){
                foreach($GLOBALS[$vn] as $name=>$value){
                    $env[$n.'_'.$name] = (string)$value;
                }
            }
        }
        return $env;
    }

    /**
     * Collect the date time expressions
     *
     * @return array
     */
    private function collect_date_time()
    {
        $tz = in_array($this->timezone, \DateTimeZone::listIdentifiers())
            ? $this->timezone : 'UTC';
        $date_time = new \DateTime('now', new \DateTimeZone($tz));
        return [
            'date_time'=>$date_time->format('Y-m-d H:i:s'),
            'year'=>$date_time->format('Y'),
            'month'=>$date_time->format('m'),
            'date'=>$date_time->format('d'),
            'hour'=>$date_time->format('H'),
            'minute'=>$date_time->format('i'),
            'second'=>$date_time->format('s'),
            'timezone'=>$tz
        ];
    }
}
