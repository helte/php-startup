<?php
namespace Helte\StartUp\System;

/**
 * @static
 * @todo Rewrtie some commends in English from Japanese for comfortableness
 * @todo ErrorLog::open() s argument interface for file class and hide the current return value (file pointer)
 */
abstract class ErrorLog
{
    /**
     * ウェブアプリケーションのログディレクトリのパス
     * 
     * @var string
     */
    private static $log_dir = '/var/tmp/helte/worldc';

    /**
     * To be friendly for administrators,
     * the timestamp for log should be localized for the administrators.
     * 
     * Only one timezone is available.
     * 
     * @var \DateTimeZone|null
     */
    private static $admin_timezone;

    /**
     * File permission (works only on Linux / Macintosh server)
     * 
     * @var int
     */
    const LOG_FILE_PERMISSION = 0777; // TODO: think of the permission

    /**
     * ログディレクトリを設定します。
     * 
     * @param string $dir
     */
    final public static function setLogDir($dir)
    {
        self::$log_dir = $dir;
    }

    /**
     * Local timezone of administrators
     *
     * @param \DateTimeZone $timezone
     */
    final public static function setTimezone(\DateTimeZone $timezone)
    {
        self::$admin_timezone = $timezone;
    }
    
    /**
     * Get the log file path
     *
     * @param string $date_expr Date expression available for \DateTime. Default is today
     * @return string
     */
    final public static function getFile($date_expr='now')
    {
        $base_name = self::get_day_file_base_name($date_expr) . '.log';
        return self::$log_dir . DIRECTORY_SEPARATOR . $base_name;
    }

    /**
     * Open the log file to operate
     *
     * NOTE: this method will highly possibly be changed its argument interface due to
     * file class.
     * 
     * @param string $fopen_mode Mode available for fopen()
     * @param string $date_expr Date expression available for \DateTime. Default is today
     * @return resource|false File resource of fopen() or false when it fails
     * @throws \Exception
     */
    final public static function open($fopen_mode, $date_expr='now')
    {
        if(in_array($fopen_mode, ['a','r','w','ab','wb','rb'])){
            $file = self::getFile($date_expr);
            $dir = dirname($file);
            if(!file_exists($file)) mkdir($dir, self::LOG_FILE_PERMISSION, true);
            return fopen($file, $fopen_mode);
        }else{
            throw new \Exception('Invalid fopen mode for '.__CLASS__.'::'.__FUNCTION__.'()');
        }
    }

    /**
     * Build the base name (without the extension) for the date
     *
     * @param string $date_expr Date expression available for \DateTime
     * @return string Base name of the file (without the extension)
     */
    final private static function get_day_file_base_name($date_expr='now')
    {
        if(is_null(self::$admin_timezone)) self::$admin_timezone = new \DateTimeZone('UTC');
        $admin_now = new \DateTime($date_expr, self::$admin_timezone);
        return $admin_now->format('Y-m-d');
    }
}
