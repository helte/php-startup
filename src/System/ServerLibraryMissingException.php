<?php
namespace Helte\StartUp\System;

/**
 * Exception to throw when a server library is missing
 */
class ServerLibraryMissingException extends \Exception
{    
    /** @var int */
    public static $errorCode = -1;
    /** @var string  */
    private $missing_lib_name;
    /** @var string  */
    private $how_to_install;

    /**
     * @param string $missing_lib_name
     * @param string $how_to_install
     * @param \Exception|null $previous
     */
    public function __construct($missing_lib_name, $how_to_install, \Exception $previous=null)
    {
        $this->missing_lib_name = $missing_lib_name;
        $this->how_to_install = $how_to_install;
        parent::__construct(sprintf('%s is missing on this server: install: %s', $missing_lib_name, $how_to_install), self::$errorCode, $previous);
    }

    /** @return string */
    public function getMissingLibName(){ return $this->missing_lib_name; }
    /** @return string */
    public function getHowToInstall(){ return $this->how_to_install; }
}
