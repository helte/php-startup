<?php
namespace Helte\StartUp\Models;

/**
 * Class for domain access control
 */
abstract class Security
{
    /**
     * List of allowed patterns in regular expression
     * 
     * @var string[]
     */
    private static $plain_allows = [];

    /**
     * List of allowed patterns in regular expression
     * 
     * @var string[]
     */
    private static $preg_allows = [];

    /**
     * Whether all accesses are allowed, or not
     * 
     * @var bool
     */
    private static $allow_all = false;

    /**
     * Allow all accesses
     */
    final public static function allowAll()
    {
        self::$allow_all = true;
    }
    
    /**
     * Add new domain in string to allow the access
     *
     * @param string $plain_allow
     */
    final public static function allowPlain($plain_allow)
    {
        self::$plain_allows[] = $plain_allow;
    }

    /**
     * Add new domain pattern in regular expression
     * to allow the access
     *
     * @param string $preg_allow
     */
    final public static function allowPreg($preg_allow)
    {
        self::$preg_allows[] = $preg_allow;
    }

    /**
     * Check whether the referer matches to any of allowed domains
     *
     * @param null|string $referer
     * @return bool
     */
    final public static function isAllowed($referer=null)
    {
        if(self::$allow_all) return true;
        
        if(!$referer) $referer = $_SERVER['HTTP_REFERER'];
        foreach(self::$plain_allows as $_origin){
            if(strpos($referer, $_origin) !== false) return true;
        }
        foreach(self::$preg_allows as $_origin){
            if(preg_match($_origin, $referer)) return true;
        }
        return false;
    }
}
