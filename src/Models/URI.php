<?php
namespace Helte\StartUp\Models;

/**
 * Deal with URI/URL string
 */
class URI
{        
    /**
     * Get the current access URI
     *
     * @return null|string
     */
    final public static function current()
    {
        return isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : null;
    }
    
    /**
     * Get the previous URI (referer URI)
     *
     * @return null|string
     */
    final public static function previous()
    {
        return isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;
    }

    /**
     * Extend the given URI with new parameters and hash
     *
     * @param string        $uri
     * @param array         $params
     * @param null|string   $hash
     * @return string
     */
    final public static function extend($uri, array $params, $hash=null)
    {
        $i = strpos($uri, '?');
        $queried = is_int($i) && $i >= 0;
        $output = $queried ? substr($uri, 0, $i) : $uri;
        $gets = [];
        if($queried) parse_str(substr($uri, $i+1), $gets);
        $new_params = \array_merge($gets, $params);
        if(!empty($new_params)) $output .= '?' . http_build_query($new_params);
        if(is_string($hash) && strlen($hash)) $output .= '#'.$hash;
        return $output;
    }
    
    /**
     * Drop the string query and get the simple URL
     * 
     * @param string $uri
     * @return string URL without query string
     */
    final public static function pure($uri)
    {
        $i = strpos($uri, '?');
        return is_int($i) && $i >= 0 ? substr($uri, 0, $i) : $uri;
    }
}
