<?php
namespace Helte\StartUp\Mailer;

use Helte\StartUp\Mailer\Mailer as StartUpMailer;
use Helte\StartUp\System\Template;

class TemplateMailer
{
    /** @var StartUpMailer */
    private $mailer;
    /** @var string */
    private $from_name;
    /** @var string */
    private $from_address;

    /**
     * Validate a string as email address
     *
     * @param string $email
     * @return bool
     */
    public static function validate($email)
    {
        return (bool)preg_match('/[^@]+@[^@]+/', $email);
    }
    
    /**
     * @param string $from_name
     * @param string $from_address
     */
    public function __construct($from_name, $from_address)
    {
        $this->mailer = StartUpMailer::getInstance();
        $this->from_name = $from_name;
        $this->from_address = $from_address;
    }

    /**
     * Do something
     *
     * @param string   $to       Email address
     * @param Template $template
     * @param array $values
     * @param array $smtp
     * @return boolean
     */
    public function send($to, Template $template, array $values=[], array $smtp=[])
    {
        $subject = $template->getContent('email/subject', $values);
        $body = $template->getContent('email/body', $values);
        return $this->mailer->send($to, $subject, $body, $this->from_name, $this->from_address, $smtp);
    }
}
