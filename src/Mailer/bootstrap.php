<?php
///
/// Autoloader only for Mailgun library (namespace=Mailgun\*)
///
spl_autoload_register(function($class_name){
    $ns = 'Mailgun';
    if(strpos($class_name, $ns) !== 0) return;
    $rel_path = strtr(substr($class_name, strlen($ns)+1), '\\', DIRECTORY_SEPARATOR);
    $file = implode(DIRECTORY_SEPARATOR, [
            __DIR__, $ns, $rel_path
        ]) . '.php';
    if(is_readable($file)) require_once $file;
});

/*
require_once dirname(__FILE__) . '/bootstrap.php';
use Mailgun\Mailgun;

# Instantiate the client.
$mgClient = new Mailgun('key-ab3984d67fdb1521bce63b2ef5240a74');
$domain = "sandboxcb5a091cfd5847a9976c2ce6882ed38a.mailgun.org";
$to = '';

if(strlen($to)){
# Make the call to the client.
    $result = $mgClient->sendMessage("$domain",
        array('from'    => 'Mailgun Sandbox <postmaster@'.$domain.'>',
            'to'      => $to,
            'subject' => 'Hello saho-planning',
            'text'    => 'Congratulations saho-planning, you just sent an email with Mailgun!  You are truly awesome!  You can see a record of this email in your logs: https://mailgun.com/cp/log .  You can send up to 300 emails/day from this sandbox server.  Next, you should add your own domain so you can send 10,000 emails/month for free.'));

    die($result ? 'Sent it' : 'Failed to send it');
}else{
    die('Please define $to as your email address to test mailgun.');
}
*/
