<?php
namespace Helte\StartUp\Mailer;

use Qdmail;
//use Mailgun\Mailgun;

/// This is always required
require_once __DIR__ . '/bootstrap.php';
require_once dirname(__FILE__) . '/qdmail/qdmail.php';
require_once dirname(__FILE__) . '/qdmail/qdmail_receiver.php';
require_once dirname(__FILE__) . '/qdmail/qdsmtp.php';

class Mailer
{
    /**
     * Validate a string as email address
     *
     * @param string $email
     * @return bool
     */
    public static function validate($email)
    {
        return (bool)preg_match('/[^@]+@[^@]+/', $email);
    }
    
    
    /**
     * @var Mailgun Object of Mailgun
     */
    private $mailgun;
    /**
     * @var string Domain associated with Mailgun API key
     */
    private $mailgun_domain;
    
    /** @var Mailer Singleton */
    private static $inst;

    /**
     * Get the instance of Mailer
     *
     * @return Mailer
     */
    final public static function getInstance()
    {
        if(!self::$inst) self::$inst = new self();
        return self::$inst;
    }
    
    /// This class is singleton; to instantiate, call getInstance()
    protected function __construct()
    {
        $this->setDefaultFromAddress(function($domain, $to){
            return 'postmaster@'.$domain.' <postmaster@'.$domain.'>';
        });
    }

    /**
     * Set to use Mailgun as send an email
     *
     * @param string $api_key
     * @param string $domain
     * @return $this
     */
    public function enableMailgun($api_key, $domain)
    {
        $this->mailgun = new Mailgun($api_key);
        $this->mailgun_domain = $domain;
        return $this;
    }

    /**
     * Send an email
     *
     * @param string $to
     * @param string $subject
     * @param string $body
     * @param string $fromname
     * @param string $fromaddress
     * @param array $smtp
     * @return mixed
     */
    public function send($to, $subject, $body, $fromname, $fromaddress, array $smtp=[])
    {
        return $this->sendQdmail($to, $subject, $body, $fromname, $fromaddress, $smtp);
        /*
        $to   = is_array($address) && isset($address['to']) ? $address['to'] : $address;
        $from = is_array($address) && isset($address['from']) ? $address['from'] : $this->get_default_from_address($this->mailgun_domain, $to);
        return $this->mailgun->sendMessage(
            $this->mailgun_domain,
            [
                'from'=>$from,
                'to'=>$to,
                'subject'=>$subject,
                'text'=>$text
            ]
        );*/
    }

    /**
     * Send email by qdmail
     *
     * @param string $to
     * @param string $subject
     * @param string $body
     * @param string $fromname
     * @param string $fromaddress
     * @param array $smtp
     * @return mixed
     */
    public function sendQdmail($to, $subject, $body, $fromname, $fromaddress, array $smtp=[])
    {
        $mail = new Qdmail();
        if(is_array($smtp) && !empty($smtp)){
            $mail->smtp(true);
            $smtp['protocol'] = 'SMTP_AUTH';
            $mail->smtpServer($smtp);
        }
        $mail ->to($to);
        $mail ->subject($subject);
        $mail ->from($fromaddress, $fromname);
        $mail ->text($body);
        return $mail ->send();
    }
    
    private $from_address_maker;
    private $from_address_static;

    /**
     * Set "from" email address for default
     *
     * @param string|callable $address String for a static email address,
     *                                 callable for a function to create email adress
     *                                 function(string $domain, string $to)
     * @return $this
     */
    public function setDefaultFromAddress($address)
    {
        if(is_callable($address)){
            $this->from_address_maker = $address;
            $this->from_address_static = null;
        }else{
            $this->from_address_maker = null;
            $this->from_address_static = $address;
        }
        return $this;
    }

    /**
     * Get the default "from" email address
     *
     * @param string $domain
     * @param string $to
     * @return string
     */
    private function get_default_from_address($domain, $to)
    {
        if(is_callable($this->from_address_maker)){
            $address = call_user_func($this->from_address_maker, $domain, $to);
            return is_string($address) ? $address : (string)$address;
        }else{
            return $this->from_address_static;
        }
    }
}
