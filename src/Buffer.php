<?php
namespace Helte\StartUp;

class Buffer
{
    /** @var int */
    private $length;
    /** @var mixed[] */
    private $data;
    /** @var int */
    private $count;

    public function __construct($length, array $initial=[])
    {
        $this->length = $length;
        $this->data = $initial;
        $this->count = count($this->data);
    }

    /**
     * Add the value
     *
     * @param mixed $value
     * @return $this
     */
    public function add($value)
    {
        $this->data[] = $value;
        $this->count++;
        if($this->on_full && $this->count === $this->length){
            $this->runOnFull();
        }
        return $this;
    }

    /**
     * Get the buffered values and clean them from the internal
     *
     * @return mixed[]
     */
    public function retrieve()
    {
        $data = $this->data;
        $this->data = [];
        $this->count = 0;
        return $data;
    }

    /**
     * Change the length
     *
     * @param int $length
     * @return $this
     */
    public function length($length=0)
    {
        if(func_num_args() === 0) return $this->length;
        $this->length = $length;
        return $this;
    }

    /**
     * Check if the buffer is full
     *
     * @return bool
     */
    public function full()
    {
        return $this->count === $this->length;
    }

    /**
     * Run the onFull event by force
     *
     * @return $this
     */
    public function push()
    {
        $this->runOnFull();
        return $this;
    }

    /**
     * Walk through each element
     *
     * @param callable $on_each
     * @param array    $args
     * @return $this|bool
     */
    public function each(callable $on_each, array $args=[])
    {
        foreach($this->data as $_offset=>$_data){
            $r = call_user_func($on_each, array_merge([$_data, $_offset], $args));
            if($r === false) return false;
        }
        return $this;
    }

    public function onFull(callable $on_full, array $args=[])
    {
        $this->on_full = [$on_full, $args];
    }
    
    private $on_full;
    
    protected function runOnFull()
    {
        return call_user_func($this->on_full[0], array_merge([$this->data, $this->length, $this], $this->on_full[1]));   
    }
}

