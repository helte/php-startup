<?php
namespace Helte\StartUp;

use Helte\StartUp\System\Server;
use Helte\StartUp\System\ServerLibraryMissingException;
use Helte\StartUp\Codes\UnknownException;
use Helte\StartUp\Concepts\Coordinate;
use Helte\StartUp\Concepts\Rect;

class Image
{
    /**
     * @var null|resource
     */
    private $image;
    /**
     * @var Rect
     */
    private $size;

    /**
     * Instantiate an object from the file
     *
     * @param string $type    "jpeg", "png", "gif"
     * @param string $file    File path of the image
     * @throws \Exception
     * @return bool|Image
     */
    public static function fromFile($type, $file)
    {
        $image_func_name = 'imagecreatefrom'.$type;
        if(function_exists($image_func_name)){
            if(($res = call_user_func($image_func_name, $file)) !== false){
                return new self($res);
            }else{
                throw new \Exception(sprintf('Image type gap: %s is not %s', basename($file), $type));
            }
        }else if(in_array($type, ['jpeg','gif', 'png'])){
            if(Server::absent(['php', 'gd'])){
                throw new ServerLibraryMissingException('php-gd', 'Check what library is available for your server by `yum list | grep php | grep gd`, and run `yum install -y (lib-name)`.');
            }else{
                throw new UnknownException(sprintf('%s is not supported for unknown reasons', $type));
            }
        }else{
            throw new \Exception('Invalid image type: ' . var_export($type, true));
        }
    }

    /**
     * Detect the image file type by binary
     *
     * @param string $file File path
     * @return int|string
     * @throws \Exception
     */
    public static function getType($file)
    {
        $info = @getimagesize($file);
        return is_string($info['mime']) ? substr($info['mime'], 6) : 'other';
    }

    /**
     * @param resource|null $image
     * @throws \InvalidArgumentException
     */
    public function __construct($image)
    {
        if(is_resource($image)) {
            $this->image = $image;
        }else{
            throw new \InvalidArgumentException(sprintf(
                '%s::%s requires an image resource for %d%s argument, but %s was given',
                __CLASS__, __METHOD__, 1, 'st', gettype($image)));
        }
    }

    /**
     * Resize the image to a new image
     *
     * @param Rect $destination  Destination width and height
     * @param bool $keep_ratio   Whether the destination height should be calculated
     * @return Image
     */
    public function resize(Rect $destination, $keep_ratio=false)
    {
        if(is_null($this->size)) $this->size();

        if($keep_ratio) $destination->setSameRatioForHeight($this->size);

        // For the same ratio rectangle, resizing is very simple.
        if($this->size->ratio() === $destination->ratio()){
            return $this->__resize($destination);
        }

        $intermediate = clone $destination;
        $diff = $this->size->diff($destination);
        $magic = $this->size->includable($destination);
        if(($diff->ratio() < $this->size->ratio()) === $magic){
            $intermediate->setSameRatioForHeight($this->size);
        }else{
            $intermediate->setSameRatioForWidth($this->size);
        }
        $intermediate_thumb = $this->__resize($intermediate);
        $crop_offset = $intermediate->getOffset($destination);
        return $intermediate_thumb->crop($destination, $crop_offset);
    }

    /**
     * Crop the image and create a new image
     *
     * NOTE: the destination size should be smaller than the source size.
     *
     * @param Rect       $size
     * @param Coordinate $offset
     * @return $this
     */
    public function crop(Rect $size, Coordinate $offset=null)
    {
        if(!$offset) $offset = new Coordinate(0 ,0);
        else         $offset = $offset->integerise();

        if(is_null($this->size)) $this->size();
        $thumb = imagecreatetruecolor($size->width, $size->height);
        imagecopyresized($thumb, $this->image,
            0, 0, $offset->x, $offset->y,
            $this->size->width, $this->size->height,
            $this->size->width, $this->size->height);

        return new self($thumb);
    }

    /**
     * Save the image as a file
     *
     * @param string $type    "jpeg", "png", "gif"
     * @param string $save_as File path to save as
     * @return bool
     */
    public function save($type, $save_as)
    {
        return call_user_func('image'.$type, $this->image, $save_as);
    }

    /**
     * Get the rectangle size of the image
     *
     * @return Rect
     */
    public function size()
    {
        $this->size = new Rect(imagesx($this->image), imagesy($this->image));
        return $this->size;
    }

    /**
     * Simply resize the image
     *
     * NOTE: the result image fits the destination ratio even if the source has
     * different ratio, which means the image ratio will be broken between different ratios.
     *
     * @param Rect $destination
     * @return Image
     * @throws \DomainException
     */
    private function __resize(Rect $destination)
    {
        if($destination->real()){
            if(is_null($this->size)) $this->size();
            $thumb = imagecreatetruecolor($destination->width, $destination->height);
            imagecopyresized($thumb, $this->image,
                0, 0, 0, 0,
                $destination->width, $destination->height,
                $this->size->width,  $this->size->height);

            return new self($thumb);
        }else{
            throw new \DomainException(sprintf(
                '%s::%s requires a real rectangle to resize the image, but actually the values are x: %d, y: %d',
                __CLASS__, __METHOD__, $destination->width, $destination->height
            ));
        }
    }

    /**
     * Saves the memory releasing the image resource data
     */
    public function __destruct()
    {
        imagedestroy($this->image);
    }
}
