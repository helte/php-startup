<?php
namespace Helte\StartUp\Output;

use Helte\StartUp\Models\Security;

class JavaScript
{
    /**
     * Output JavaScript content
     *
     * @param callable $inner        Writer function to write the content in JavaScript
     * @param string   $content_type You can define ContentType by yourself
     */
    final public static function output(callable $inner, $content_type='text/application+json')
    {
        $params = parse_url(isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'https://www.google.co.jp/search');
        @header('Access-Control-Allow-Origin: ' . 'https://'.$params['host']);
        @header('Access-Control-Allow-Credentials: true');

        // Main
        @header('Content-type: '.$content_type.'; charset=utf-8');
        @header("Cache-Control: no-cache, must-revalidate");
        @header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

        call_user_func($inner);
        exit(0);
    }
}
