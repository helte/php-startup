<?php
namespace Helte\StartUp\Output;

use Helte\StartUp\System\Server;

/**
 * Control the content of and how to output the data
 */
abstract class Json
{
    /** @var bool True to treat the current user as an administrator to show detail information */
    private static $as_admin = false;

    /** Set the output mode for an administrator to give detailed information */
    public static function setAsAdministator()
    {
        self::$as_admin = true;
    }

    /** Unset the output mode for an administrator NOT to give detailed information */
    public static function setAsGuest()
    {
        self::$as_admin = false;
    }

    /**
     * Output the data in Json format
     *
     * @param array           $data   Data to output in Json format
     * @param \Exception|null $culprit Culprit exception which causes the output when error
     *                                 This parameter is only for administrators in debug parameter.
     */
    public static function output(array $data=[], \Exception $culprit=null)
    {
        self::beforeFilter($data);

        if(Server::local() || Server::test()){
            $trace = debug_backtrace();
            if(!isset($data['debug'])) $data['debug'] = [];
            $data['debug'] = array_merge($data['debug'], [
                'file'=>$trace[0]['file'],
                'line'=>$trace[0]['line'],
                'error'=>error_get_last(),
                'POST'=>$_POST,
                'GET'=>$_GET,
                'FILES'=>$_FILES,
                'SERVER'=>$_SERVER
            ]);
        }else if(isset($data['debug'])){
            unset($data['debug']);
        }

        if(self::$as_admin && $culprit !== null){
            $data['debug']['exception'] = [
                'code'=>$culprit->getCode(),
                'message'=>$culprit->getMessage(),
                'file'=>$culprit->getFile(),
                'line'=>$culprit->getLine(),
                'trace'=>$culprit->getTrace()
            ];
        }

        self::afterFilter($data);

        // Main
        JavaScript::output(function()use($data){
            echo json_encode($data);
        });
    }

    private static function beforeFilter(array &$data)
    {
        if(!isset($data['error'])) $data['error'] = 0;
        if(!isset($data['message'])) $data['message'] = 'ok';
    }

    private static function afterFilter(array &$data)
    {
        global $currentUser;
        if(isset($currentUser)) $data['currentUser'] = $currentUser;
        //save_log(serialize($data)); // TODO
    }
}
