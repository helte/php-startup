<?php
namespace Helte\StartUp;

/**
 * Operate a string in an object
 */
class Text
{
    /**
     * @var string
     */
    private $raw;
    /**
     * @var int
     */
    private $length;


    /**
     * Check if the text is shorter than the given number
     *
     * @param string|Text $text
     * @param int|float|double $longer
     * @return bool
     */
    public static function short($text, $longer)
    {
        return strlen($text) < $longer;
    }

    /**
     * Check if the text is longer than the given number
     *
     * @param string|Text $text
     * @param int|float|double $shorter
     * @return bool
     */
    public static function long($text, $shorter)
    {
        return strlen($text) > $shorter;
    }

    /**
     * Check if the text length is in the range of the given numbers
     *
     * @param string|Text $text
     * @param int|float|double $min
     * @param int|float|double $max
     * @return bool
     */
    public static function fit($text, $min, $max)
    {
        return Number::between(strlen($text), $min, $max);
    }
    
    
    /**
     * @param string|Text $raw
     */
    public function __construct($raw='')
    {
        if(is_object($raw) && $raw instanceof Text){
            $this->raw = $raw->raw;
            $this->length = $raw->length;
        }else{
            $this->raw = (string)$raw;
            $this->length = -1;
        }
    }

    /**
     * Check if the raw value is equivalent to the raw of the given value
     *
     * @param string|Text $raw
     * @return bool
     */
    public function eq($raw)
    {
        if(is_object($raw) && $raw instanceof Text){
            return $this->raw === $raw->raw;
        }else{
            return $this->raw === (string)$raw;
        }
    }

    /**
     * Overwrite the text
     * 
     * @param string|Text $raw
     * @return $this
     */
    public function overwrite($raw)
    {
        if(is_object($raw) && $raw instanceof Text){
            $this->raw = $raw->raw;
            $this->length = $raw->length;
        }else{
            $this->raw = (string)$raw;
            $this->length = -1;
        }
        return $this;
    }

    /**
     * Prepend a string before the text
     *
     * @param  string|Text $str
     * @return $this
     */
    public function prepend($str)
    {
        $this->raw = (string)$str . $this->raw;
        return $this;
    }

    /**
     * Append a string after the text
     *
     * @param  string|Text $str
     * @return $this
     */
    public function append($str)
    {
        $this->raw .= (string)$str;
        return $this;
    }

    /**
     * Get the string before the needle
     *
     * @param string $needle
     * @param int    $offset
     * @return Text
     */
    public function before($needle, $offset=0)
    {
        $i = strpos($this->raw, $needle, $offset);
        return new Text($i === false ? $this->raw : substr($this->raw, 0, $i));
    }

    /**
     * Get the string after the needle
     *
     * @param string $needle
     * @param int    $offset
     * @return Text
     */
    public function after($needle, $offset=0)
    {
        $i = strpos($this->raw, $needle, $offset);
        return new Text($i === false ? '' : substr($this->raw, $i));
    }

    /**
     * Check if the needle is in the string
     *
     * @param string $needle
     * @param int    $offset
     * @return bool
     */
    public function has($needle, $offset=0)
    {
        return strpos($this->raw, $needle, $offset) !== false;
    }

    /**
     * Get the part of the string within the given place
     *
     * @param int      $offset
     * @param int|null $length
     * @return Text
     */
    public function sub($offset=0, $length=null)
    {
        $sub = clone $this;
        return $sub->cut($offset, $length);
    }

    /**
     * Cut the text to only the part of the string within the given place
     *
     * @param int      $offset
     * @param int|null $length
     * @return $this
     */
    public function cut($offset=0, $length=null)
    {
        if(!is_int($length)){
            $this->raw = is_int($offset) ? substr($this->raw, $offset) : '';
        }else{
            $this->raw = substr($this->raw, $offset, $length);
        }
        if(!is_string($this->raw)) $this->raw = '';
        return $this;
    }

    /**
     * Split the text into the list of terms by the delimiter
     *
     * @param string|Text $delimiter
     * @return Stack
     */
    public function split($delimiter)
    {
        return new Stack(array_map(function($str){
            return new Text($str);
        }, explode((string)$delimiter, $this->raw)));
    }

    /**
     * Get the text character length
     *
     * @return int
     */
    public function length()
    {
        if($this->length < 0){
            $this->length = strlen($this->raw);
        }
        return $this->length;
    }

    /** @return string */
    public function __toString(){ return $this->raw; }
}
