<?php
namespace Helte\StartUp\Databases\MySQL;

require_once __DIR__ . '/../DataSource.php';
require_once __DIR__ . '/../SQLDatabase.php';
require_once __DIR__ . '/MySQL.php';

new MySQLTest;

class MySQLTest
{
    /** @var SQLServer */
    private $conn;

    function __construct()
    {
        $this->conn = new MySQL;
        $host = 'home.lan';
        $port = '3306';
        $user = 'ohtamasato';
        $password = '84190120';
        $db_name = 'test';

        $this->conn->connect($host, $user, $password, $db_name, ['port'=>$port]);
        $now = time();
        $table = 'test_' . time();
        $ok = $this->conn->create($table, ['id bigint auto_increment', 'time int', 'primary key(id)'], ['auto_increment=1']);
        var_dump($this->conn->getError());
        $this->conn->insert($table, ['time'=>time()]);
        $this->conn->insert($table, ['time'=>$now]);
        $a = $this->conn->select($table, ['time'=>$now]);
        var_dump($a);
        $id = $this->conn->insert($table, ['time'=>$now]);
        $a = $this->conn->select($table, ['time'=>$now]);
        var_dump($a);
        $this->conn->delete($table, ['id'=>$id]);
        $a = $this->conn->select($table, ['id'=>['op'=>'!=','value'=>'0']]);
        var_dump($a);
        $this->conn->update($table, ['id'=>1], ['time'=>$now]);
        $this->conn->select($table, ['time'=>$now]);
        $this->conn->insert($table, ['time'=>$now]);
        $user_table = 'users';
        $this->conn->create($user_table, ['id bigint auto_increment', 'time int', 'primary key(id)'], ['auto_increment=1']);
        $user_id = $this->conn->insert($user_table, ['time'=>$now]);
        $a = $this->conn->select([
            'table'=>$table,
            'join'=>[
                'type'=>'left',
                'name'=>$user_table,
                'on'=>$table.'.id='.$user_table.'.id'
            ]], [$user_table.'.id'=>$user_id]);
        var_dump($a);
        $this->conn->drop($user_table);
        $this->conn->drop($table);
    }

    public function testCreate()
    {
        $table = 'test_' . time();
        $this->assertEquals(true, $this->conn->create($table, ['id bigint', 'time int'], []));
        $this->assertEquals(true, $this->conn->drop($table));
    }

    /**
     */
    public function testInsert()
    {
        $this->assertEquals(true, $this->conn->insert('test', ['time'=>time()]) > 0);
    }

    /**
     */
    public function testSelect()
    {
        $now = time();
        $this->conn->insert('test', ['time'=>$now]);
        $res = $this->conn->select('test', ['time'=>$now]);
        $this->assertEquals($now, is_array($res) ? $res[0]['time'] : null);
    }

    /**
     */
    public function testDelete()
    {
        $now = time();
        $id = $this->conn->insert('test', ['time'=>$now]);
        $this->assertEquals(true, $this->conn->delete('test', ['id'=>$id]));
        $res = $this->conn->select('test', ['time'=>$now]);
        $this->assertEquals(true, is_array($res) && empty($res));
    }

    /**
     */
    public function testUpdate()
    {
        $now = time();
        $this->assertEquals(true, $this->conn->update('test', ['id'=>1], ['time'=>$now]));
        $this->conn->insert('test', ['time'=>$now]);
        $res = $this->conn->select('test', ['time'=>$now]);
        $this->assertEquals($now, is_array($res) ? $res[0]['time'] : null);
    }

    public function testSelect2()
    {
        $now = time();
        $this->conn->insert('test', ['time'=>$now]);
        $res = $this->conn->select([
            'table'=>'test',
            'join'=>[
                'type'=>'left',
                'name'=>'users',
                'on'=>'test.id=users.id'
            ]], ['users.id'=>1]);
        $this->assertEquals(true, is_array($res) && is_numeric($res[0]['time']));
    }
}
