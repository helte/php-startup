<?php
namespace Helte\StartUp\Databases\MySQL;

use Helte\StartUp\Databases\PDOWrapper;

class MySQL extends PDOWrapper
{
    /**
     * {@inheritDoc}
     */
    protected function getDSN($host, $db_name, array $options=[])
    {
        $port = isset($options['port']) ? $options['port'] : 3306;
        $dsn = "mysql:host={$host}";
        if($db_name) $dsn .= "; dbname={$db_name}";
        $dsn .= "; charset=utf8";
        return $dsn;
    }

    /**
     * {@inheritDoc}
     */
    public function getTables()
    {
        $names = new \ArrayObject();
        $result = $this->query("SHOW TABLES");
        foreach($result as $row){
            $row = array_values($row);
            $names[] = $row[0];
        }
        return $names->getArrayCopy();
    }

    /**
     * {@inheritDoc}
     */
    public function getFields($table, \ArrayObject &$max_lengths = null)
    {
        $accepted_keys = new \ArrayObject();
        $max_lengths = new \ArrayObject();
        $result = $this->query(sprintf('SHOW COLUMNS FROM `%s`', $table));
        foreach($result as $row){
            $accepted_keys[] = $row['Field'];
            $info = self::parse_column_type_values($row['Type']);
            $max_lengths[$row['Field']] = $info['length'];
        }
        return $accepted_keys->getArrayCopy();
    }

    /**
     * {@inheritDoc}
     */
    protected function order_limit_sensitive($rows, $page, $order, $key)
    {
        return 'ORDER BY ' . $key . ' ' . $order.
        ' LIMIT ' . ($page*$rows) . ',' . $rows;
    }
    
    

    /**
     * "SHOW COLUMNS FROM"で取得した行データのうち"Type"の文字列を正常にパースして
     * 得られる情報を取得します。
     *
     * @param string $type
     * @return array
     *     "name": フィールドの型名
     *     "length": フィールドに確保されたデータ長
     */
    final private static function parse_column_type_values($type)
    {
        $digits = explode(' ', $type);
        $type_name = array_shift($digits);
        $info = [
            'name'=>null,
            'length'=>65535
        ];
        if(preg_match('/^(.*)\(([0-9]+)\)$/i', $type_name, $matches)){
            $info['name']   = $matches[1];
            $info['length'] = (int)$matches[2];
        }else{
            $info['name']   = $type_name;
        }
        return $info;
    }

    /**
     * GROUP BY clause
     *
     * @param string $col_name
     * @return string
     */
    protected function group_by($col_name)
    {
        return 'GROUP BY `'.$col_name.'`';
    }
}
