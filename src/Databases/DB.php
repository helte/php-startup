<?php
namespace Helte\StartUp\Databases;


/**
 * Singleton database controller
 * @static
 */
abstract class DB
{
    /** @var DataSource|null */
    private static $data_source;

    /**
     * Set the default data source object
     *
     * @param DataSource $data_source
     */
    final public static function setDataSource(DataSource $data_source)
    {
        self::$data_source = $data_source;
    }

    /**
     * @return DataSource|null
     */
    final public static function getDataSource()
    {
        return self::$data_source;
    }


    /**
     * Close the connection of the data source
     */
    final public static function close()
    {
        if (self::$data_source) self::$data_source->close();
    }


    /**
     * Perform insert a new row data
     *
     * @param string $table
     * @param array $data
     * @param bool|false $ignore
     * @return int
     */
    final public static function insert($table, array $data, $ignore = false)
    {
        if (self::$data_source) return self::$data_source->insert($table, $data, $ignore);
        return -1;
    }

    /**
     * Perform insert a new row data
     *
     * @param string $table
     * @param array $data_list
     * @param bool|false $ignore
     * @return int
     */
    final public static function inserts($table, array $data_list, $ignore = false)
    {
        if (self::$data_source) return self::$data_source->inserts($table, $data_list, $ignore);
        return -1;
    }

    /**
     * @param $table
     * @param array $conditions
     * @param array|null $fields
     * @param array $order_limit
     * @return array|false
     */
    final public static function select($table, array $conditions=[], array $fields=null, array $order_limit=[])
    {
        if (self::$data_source) return self::$data_source->select($table, $conditions, $fields, $order_limit);
        return false;
    }

    /**
     * @param $table
     * @param array $conditions
     * @param array|null $fields
     * @param array $order_limit
     * @return array|false
     */
    final public static function first($table, array $conditions=[], array $fields=null, array $order_limit=[])
    {
        if (self::$data_source){
            $records = self::$data_source->select($table, $conditions, $fields, $order_limit);
            return $records && is_array($records) ? $records[0] : false;
        }
        return false;
    }

    /**
     * @param string $table
     * @param array $conditions
     * @return int
     */
    final public static function count($table, array $conditions=[])
    {
        if (self::$data_source){
            $records = self::$data_source->select($table, $conditions);
            return $records && is_array($records) ? count($records) : 0;
        }
        return -1;
    }

    /**
     * @param string $table
     * @param array  $conditions
     * @param string $needle
     * @return array
     */
    final public static function counts($table, $groupby, array $conditions=[], $needle='id')
    {
        if (self::$data_source){
            return self::$data_source->counts($table, $groupby, $conditions, $needle);
        }
        return false;
    }

    /**
     * @param string $table
     * @param array $conditions
     * @return bool
     */
    final public static function exists($table, array $conditions=[])
    {
        return self::count($table, $conditions) > 0;
    }

    /**
     * Update data
     *
     * @param string $table
     * @param array $conditions
     * @param array $data
     * @return bool
     */
    final public static function update($table, array $conditions, array $data)
    {
        if (self::$data_source) return (bool)self::$data_source->update($table, $conditions, $data);
        return false;
    }

    /**
     * Delete data
     *
     * @param $table
     * @param array $data
     * @return bool
     */
    final public static function delete($table, array $data)
    {
        if (self::$data_source) return (bool)self::$data_source->delete($table, $data);
        return false;
    }

    /**
     * @param $table
     * @param string[]   $fields
     * @param string[]   $options
     * @param bool|false $force
     * @return bool
     */
    final public static function create($table, array $fields, array $options=[], $force = false)
    {
        if (self::$data_source) return (bool)self::$data_source->create($table, $fields, $options, $force);
        return false;
    }

    /**
     * @param $table
     * @return bool
     */
    final public static function drop($table)
    {
        if (self::$data_source) return (bool)self::$data_source->drop($table);
        return false;
    }

    /**
     * Get the list of table names
     *
     * @return string[]
     */
    final public static function getTables()
    {
        if (self::$data_source) return self::$data_source->getTables();
        return false;
    }


    /**
     * @param string     $table       Table name in string
     * @param array|null $max_lengths (Optional) if you give a variable, you can get the array object
     *                                 of max length list for the return keys
     * @return string[]
     */
    final public static function getFields($table, array &$max_lengths=null)
    {
        if (self::$data_source) return self::$data_source->getFields($table, $max_lengths);
        return false;
    }
}
