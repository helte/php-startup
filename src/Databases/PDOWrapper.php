<?php
namespace Helte\StartUp\Databases;


abstract class PDOWrapper extends SQLDatabase
{
    /** @var \PDO */
    private $conn;

    /**
     * @deprecated
     * @return \PDO
     */
    final public function getRawConnection()
    {
        return $this->conn;
    }

    /**
     * PDOでデータソースに接続するための文字列を取得します。
     *
     * @param string $host    データソースサーバーのホスト名
     * @param string $db_name データベース名
     * @param array  $options 様々なオプション値を指定できます。
     * @return $this
     */
    abstract protected function getDSN($host, $db_name, array $options=[]);
    
    /**
     * {@inheritDoc}
     */
    public function connect($host, $user, $pass, $db_name = null, array $options = [])
    {
        $this->conn = new \PDO ($this->getDSN($host, $db_name, $options), $user, $pass);
        $this->conn->setAttribute( \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function close()
    {
        $this->conn = null;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function insert($table, array $data, $ignore=false)
    {
        return parent::insert($table, $data, false);
    }

    /**
     * {@inheritDoc}
     */
    protected function escape($str)
    {
        return $this->conn->quote($str);
    }

    /**
     * {@inheritDoc}
     */
    protected function lastInsertId()
    {
        return $this->conn->lastInsertId();
    }

    /**
     * {@inheritDoc}
     */
    protected function query($sql)
    {
        parent::query($sql);
        return $this->conn->query($sql);
    }
}
