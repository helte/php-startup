<?php
namespace Helte\StartUp\Databases\Microsoft;


class SQLServerTest extends \PHPUnit_Framework_TestCase
{
    /** @var SQLServer */
    private $conn;

    protected function setUp()
    {
        parent::setUp();
        $this->conn = new SQLServer;
        $host = 'helte-worldc.database.windows.net';
        $port = '1433';
        $user = 'ohtamasato';
        $password = 'f8c4K1G9';
        $db_name = 'worldc';

        $this->conn->connect($host, $user, $password, $db_name, ['port'=>$port]);
    }

    public function testCreate()
    {
        $table = 'test_' . time();
        $this->assertEquals(true, $this->conn->create($table, ['id bigint', 'time int'], []));
        $this->assertEquals(true, $this->conn->drop($table));
    }

    /**
     */
    public function testInsert()
    {
        $this->assertEquals(true, $this->conn->insert('test', ['time'=>time()]) > 0);
    }

    /**
     */
    public function testSelect()
    {
        $now = time();
        $this->conn->insert('test', ['time'=>$now]);
        $res = $this->conn->select('test', ['time'=>$now]);
        $this->assertEquals($now, is_array($res) ? $res[0]['time'] : null);
    }

    /**
     */
    public function testDelete()
    {
        $now = time();
        $id = $this->conn->insert('test', ['time'=>$now]);
        $this->assertEquals(true, $this->conn->delete('test', ['id'=>$id]));
        $res = $this->conn->select('test', ['time'=>$now]);
        $this->assertEquals(true, is_array($res) && empty($res));
    }

    /**
     */
    public function testUpdate()
    {
        $now = time();
        $this->assertEquals(true, $this->conn->update('test', ['id'=>1], ['time'=>$now]));
        $this->conn->insert('test', ['time'=>$now]);
        $res = $this->conn->select('test', ['time'=>$now]);
        $this->assertEquals($now, is_array($res) ? $res[0]['time'] : null);
    }

    public function testSelect2()
    {
        $now = time();
        $this->conn->insert('test', ['time'=>$now]);
        $res = $this->conn->select([
            'table'=>'test',
            'join'=>[
                'type'=>'left',
                'name'=>'users',
                'on'=>'test.id=users.id'
            ]], ['users.id'=>1]);
        $this->assertEquals(true, is_array($res) && is_numeric($res[0]['time']));
    }
}
