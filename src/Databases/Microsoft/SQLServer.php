<?php
namespace Helte\StartUp\Databases\Microsoft;

use Helte\StartUp\Databases\PDOWrapper;

class SQLServer extends PDOWrapper
{
    /**
     * PDOでデータソースに接続するための文字列を取得します。
     *
     * @param string $host    データソースサーバーのホスト名
     * @param string $db_name データベース名
     * @param array  $options 様々なオプション値を指定できます。
     * @return $this
     */
    protected function getDSN($host, $db_name, array $options=[])
    {
        $port = isset($options['port']) ? $options['port'] : 3306;
        $dsn = "sqlsrv:server=tcp:{$host},{$port}";
        if($db_name) $dsn .= "; Database={$db_name}";
        return $dsn;
    }

    /**
     * {@inheritDoc}
     */
    public function getTables()
    {
        $names = new \ArrayObject();
        $result = $this->query("select name from sysobjects where xtype = 'U'");
        foreach($result as $row){
            $names[] = $row['name'];
        }
        return $names->getArrayCopy();
    }

    /**
     * {@inheritDoc}
     */
    public function getFields($table, \ArrayObject &$max_lengths = null)
    {
        $accepted_keys = new \ArrayObject();
        $max_lengths = new \ArrayObject();
        $result = $this->query(sprintf(
            'SELECT name, max_length FROM sys.columns WHERE object_id = OBJECT_ID(\'dbo.%s\') ', $table));
        foreach($result as $row){
            $accepted_keys[] = $row['name'];
            $max_lengths[$row['name']] = (int)$row['max_length'];
        }
        return $accepted_keys->getArrayCopy();
    }

    /**
     * {@inheritDoc}
     */
    protected function order_limit_sensitive($rows, $page, $order, $key)
    {
        return 'ORDER BY ' . $key . ' ' . $order.
                ' OFFSET ' . ($page*$rows) . ' ROWS'.
                ' FETCH NEXT ' . $rows . ' ROWS ONLY';
    }
}
