<?php
namespace Helte\StartUp\Databases;

abstract class Table
{
    /**
     * @return array|string
     */
    abstract  public function getComplex();
}