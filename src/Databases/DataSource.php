<?php
namespace Helte\StartUp\Databases;

/**
 * Interface which all database / data-source classes strictly have to follow.
 */
interface DataSource
{
    /**
     * Set a query recorder
     *
     * @param callable $query_recorder
     * @return $this
     */
    public function setQueryRecorder($query_recorder);
    
    /**
     * @param string      $host    Database server host name
     * @param string      $user    Database login account
     * @param string      $pass    Database login account's raw password
     * @param null|string $db_name Database name to work on soon
     * @param array       $options Database option flag
     * @return $this
     */
    public function connect($host, $user, $pass, $db_name=null, array $options=[]);

    /**
     * Count some things grouped by
     * 
     * @param string|array    $table
     * @param string|string[] $group_by    String for simply a field, string[] for alias and real field name
     * @param array           $conditions  See DB::select()
     * @param string          $needle      Field to count e.g. id for count(id)
     * @return array[]
     */
    public function counts($table, $group_by, array $conditions=[], $needle='id');

    /**
     * Actually not connect soon, as on needed, it automatically connects
     *
     * This can avoid unnecessary connection to a data source and reduce I/O necks.
     *
     * @param string      $host    Database server host name
     * @param string      $user    Database login account
     * @param string      $pass    Database login account's raw password
     * @param null|string $db_name Database name to work on soon
     * @param array       $options Database option flag
     * @return $this
     */
    public function delayConnect($host, $user, $pass, $db_name=null, array $options=[]);

    /**
     * @return $this
     */
    public function close();

    /**
     * Add a record of data onto the table
     *
     * @param string     $table  Existing table name
     * @param array      $data   Data: keys are field names, values are their values
     * @param bool|false $ignore
     * @return int
     */
    public function insert($table, array $data, $ignore=false);

    /**
     * Add a record of data onto the table
     *
     * @param string     $table      Existing table name
     * @param array      $data_list  Data: keys are field names, values are their values
     * @param bool|false $ignore
     * @return int
     */
    public function inserts($table, array $data_list, $ignore=false);

    /**
     * Get records hit by conditions on the table
     *
     * @param string        $table       Existing table name
     * @param array         $conditions  Search needles (AND operation)
     * @param string[]|null $fields      Specify fields to get
     * @param array         $order_limit Order and limit clause information in array
     *                                   Keys should be: int "page", int "rows", string "order", string "key"
     * @return array|false
     */
    public function select($table, array $conditions=[], array $fields=null, array $order_limit=[]);

    /**
     * Update the record with the given data on the table
     *
     * @param string $table       Existing table name
     * @param array  $conditions  Search needles (AND operation)
     * @param array  $data        Data: keys are field names, values are their values
     * @return bool
     */
    public function update($table, array $conditions, array $data);

    /**
     * Delete hit record(s) on the table
     *
     * @param string $table       Existing table name
     * @param array  $conditions  Search needles (AND operation)
     * @return bool
     */
    public function delete($table, array $conditions);

    /**
     * Create table
     *
     * @param string     $table   New table name to create
     * @param string[]   $fields  Field setting list in string list
     * @param string[]   $options Primary key and any other options for the table
     * @param bool|false $force   True to drop it before creation if the table has already created
     * @return bool
     */
    public function create($table, array $fields, array $options=[], $force = false);

    /**
     * Delete the table
     *
     * @param string $table Existing table name
     * @return bool
     */
    public function drop($table);

    /**
     * Get the list of table names
     *
     * @return string[]
     */
    public function getTables();

    /**
     * @param string            $table       Table name in string
     * @param \ArrayObject|null $max_lengths (Optional) if you give a variable, you can get the array object
     *                                 of max length list for the return keys
     * @return string[]
     */
    public function getFields($table, \ArrayObject &$max_lengths=null);

}
