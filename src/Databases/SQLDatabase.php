<?php
namespace Helte\StartUp\Databases;

/**
 * Base class for SQL database class
 */
abstract class SQLDatabase implements  DataSource
{
    /**
     * Query recorder
     * 
     * @var callable
     */
    private $query_recorder;

    /**
     * Is connected to the database
     *
     * @var boolean False to wait to call ::connect()
     */
    private $connected = true;
    /**
     * List of arguments for ::connect()
     *
     * @var mixed[]
     */
    private $delay_connect_args;
    
    /**
     * Execute a query onto SQL Sever
     *
     * @param string $sql SQL Server query
     * @return object
     */
    protected function query($sql)
    {
        if(!$this->connected){
            call_user_func_array([$this, 'connect'], $this->delay_connect_args);
            $this->connected = true;
        }
        $this->recordQuery($sql);
        return false;
    }
    
    public function counts($table, $group_by, array $conditions=[], $needle='id')
    {
        $where = $this->where($conditions);
        $sql = sprintf('SELECT %s, count(%s) AS `count` FROM %s %s GROUP BY `%s`',
            is_array($group_by) ? $group_by[0] : $group_by,
            strpos($needle,'.')!==false ? $needle : '`'.$needle.'`',
            $this->table($table),
            empty($where) ? '' : 'WHERE '.$where,
            is_array($group_by) ? $group_by[1] : $group_by);
        $result = $this->query($sql);
        if(is_bool($result)) return $result;
        $out = [];
        foreach($result as $row) $out[] = array_filter($row, function($key){
            return is_string($key);
        }, ARRAY_FILTER_USE_KEY);
        return $out;
    }

    /**
     * {@inheritDoc}
     */
    public function delayConnect($host, $user, $pass, $db_name=null, array $options=[])
    {
        $this->connected = false;
        $this->delay_connect_args = func_get_args();
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    final public function setQueryRecorder($query_recorder)
    {
        $this->query_recorder = $query_recorder;
    }

    /**
     * {@inheritDoc}
     */
    public function create($table, array $fields, array $options=[], $force = false)
    {
        if($force) $this->drop($table);
        $sql = \sprintf(
            'CREATE TABLE %s %s (%s)%s',
            $force ? '' : 'IF NOT EXISTS',
            $table,
            \implode(',', $fields),
            \implode(', ', $options));
        return $this->query($sql);
    }

    /**
     * {@inheritDoc}
     */
    public function drop($table)
    {
        $sql = \sprintf('DROP TABLE %s', $table);
        return $this->query($sql);
    }

    /**
     * {@inheritDoc}
     */
    public function select($table, array $conditions=[], array $fields = null, array $order_limit=[])
    {
        $where = $this->where($conditions);
        $sql = sprintf('SELECT %s FROM %s %s %s',
            $this->fields($fields),
            $this->table($table),
            empty($where) ? '' : 'WHERE '.$where,
            $this->orderLimit($order_limit));
        $result = $this->query($sql);
        if(is_bool($result)) return $result;
        $out = [];
        foreach($result as $row) $out[] = array_filter($row, function($key){
            return is_string($key);
        }, ARRAY_FILTER_USE_KEY);
        return $out;
    }

    /**
     * {@inheritDoc}
     */
    public function insert($table, array $data, $ignore=false)
    {
        $keys   = array_keys($data);
        $values = array_values($data);
        foreach($values as &$_value){
            if(is_null($_value)){
                $_value = 'NULL';
            }else{
                $_value = $this->escape($_value);
            }
        }

        $sql = sprintf("INSERT %s INTO %s (%s)VALUES(%s)",
            $ignore ? 'IGNORE' : '',
            $table,
            implode(', ', $keys),
            implode(', ', $values));
        $ok = $this->query($sql);
        return $ok ? $this->lastInsertId() : -1;
    }

    /**
     * {@inheritDoc}
     */
    public function inserts($table, array $data_list, $ignore=false)
    {
        if(empty($data_list)) return 0;
        $keys = array_keys($data_list[0]);
        $values_list = [];
        foreach($data_list as $_x){
            $_a = [];
            foreach($keys as $_key){
                $_a[$_key] = is_null($_x[$_key]) ? 'NULL' : $this->escape($_x[$_key]);
            }
            $values_list[] = '('.implode(',',$_a).')';
        }
        
        $sql = sprintf("INSERT %s INTO %s (%s)VALUES",
            $ignore ? 'IGNORE' : '',
            $table,
            implode(', ', $keys),
            implode(', ', $values_list));
        $ok = $this->query($sql);
        return $ok ? $this->lastInsertId() : -1;
    }

    /**
     * {@inheritDoc}
     */
    public function update($table, array $conditions, array $data)
    {
        $sql = sprintf('UPDATE %s SET %s WHERE %s', $table, $this->set($data), $this->where($conditions));
        return $this->query($sql) ? true : false;
    }

    /**
     * {@inheritDoc}
     */
    public function delete($table, array $conditions)
    {
        $sql = sprintf('DELETE FROM %s WHERE %s', $table, $this->where($conditions));
        return $this->query($sql) ? true : false;
    }

    /**
     * In case when this object is serialized
     *
     * It never keeps the connection to the server,
     * so it closes the connection before serialization.
     */
    function __sleep()
    {
        $this->close();
    }

    /**
     * Record an SQL query onto the query recorder
     *
     * @param string $sql
     * @return bool Success or not
     */
    protected function recordQuery($sql)
    {
        if(is_callable($this->query_recorder)){
            return call_user_func($this->query_recorder, $sql);
        }
        return true;
    }
    
    /**
     * Escape the given string and wrap with single quotations
     *
     * @param string $str
     * @return string
     */
    abstract protected function escape($str);

    /**
     * @return int
     */
    abstract protected function lastInsertId();

    /**
     * @param array $data
     * @return string
     */
    protected function set(array $data)
    {
        $sets = [];
        foreach($data as $name=>$value){
            if(is_null($value)){
                $sets[] = sprintf('%s=NULL', $name);
            }else{
                $sets[] = sprintf('%s=%s', $name, $this->escape($value));
            }
        }
        return implode(', ', $sets);
    }

    /**
     * @param string|array $table
     * @return string
     */
    protected function table($table)
    {
        if(is_array($table)){
            if(isset($table['table'], $table['join'])){
                return $this->join(
                    isset($table['join']['type']) ? $table['join']['type'] : 'left',
                    $this->table($table['table']),
                    $table['join']['name'],
                    $table['join']['on']);
            }else{
                throw new \DomainException('join table requires "table" and "join" parameters');
            }
        }
        return (string)$table;
    }

    /**
     * @param array $fields
     * @return string
     */
    protected function fields(array $fields=null)
    {
        if(!$fields) return '*';
        return implode(', ', $fields);
    }

    /**
     * @param string      $type
     * @param string      $table_a
     * @param string      $table_b
     * @param string      $on
     * @return string
     */
    protected function join($type, $table_a, $table_b, $on)
    {
        return sprintf('%s %s JOIN %s ON %s',
            strpos($table_a, ' ') ? '('.$table_a.')' : $table_a,
            strtoupper($type),
            $table_b,
            $on
        );
    }

    /**
     * @param array $conditions
     * @return string
     */
    protected function where(array $conditions)
    {
        $and = [];
        foreach($conditions as $name=>$value){
            $op = '=';
            if(is_array($value)){
                if(isset($value['op'])) $op = strtoupper($value['op']);
                $value = $value['value'];
            }
            switch($op){
                case 'BETWEEN':
                    if(is_array($value)){
                        $value = array_values($value);
                        $and[] = sprintf('(%s >= %s AND %s <= %s)', $name, $this->escape($value[0]), $name, $this->escape($value[1]));
                    }
                break;
                case 'NOT BETWEEN':
                    if(is_array($value)){
                        $value = array_values($value);
                        $and[] = sprintf('(%s < %s OR %s > %s)', $name, $this->escape($value[0]), $name, $this->escape($value[1]));
                    }
                    break;
                case 'LIKE':
                    $and[] = sprintf('%s LIKE %s', $name, strtr($this->escape($value), '*', '%'));
                    break;
                case 'NOT LIKE':
                    $and[] = sprintf('%s NOT LIKE %s', $name, strtr($this->escape($value), '*', '%'));
                    break;
                case 'IS NULL':
                    $and[] = sprintf('%s IS NULL', $name);
                    break;
                case 'IS NOT NULL':
                    $and[] = sprintf('%s IS NOT NULL', $name);
                    break;
                case 'OR':
                    if(is_array($value)){
                        $or = [];
                        foreach($value as $_value){
                            $or[] = sprintf('%s %s %s', $name, $_value['op'], $_value['op'] === 'IS' ? $_value['value'] : $this->escape($_value['value']));
                        }
                        $and[] = '(('.implode(') OR (', $or).'))';
                    }else{
                        $and[] = sprintf('%s %s %s', $name, $op, $this->escape($value));
                    }
                    break;
                default:
                    $and[] = sprintf('%s %s %s', $name, $op, $this->escape($value));
                    break;
            }
        }
        return implode(' AND ', $and);
    }

    /**
     * Get the LIMIT clause
     *
     * @param array $order_limit
     * @return string
     *
     * NOTE:
     * To modify the operation for this, overwrite ::order_limit_sensitive()
     * instead of this method.
     */
    final protected function orderLimit(array $order_limit)
    {
        if(!isset($order_limit['page'], $order_limit['rows'])) return '';
        if(!is_numeric($order_limit['page']) || !is_numeric($order_limit['rows'])) return '';

        if(!isset($order_limit['order']) || !in_array(strtoupper($order_limit['order']), ['ASC', 'DESC'])) $order_limit['order'] = 'ASC';
        if(!isset($order_limit['key'])) $order_limit['key'] = 'id';

        $page = (int)$order_limit['page'];
        $rows = (int)$order_limit['rows'];

        return $this->order_limit_sensitive($rows, $page, $order_limit['order'], $order_limit['key'])
            . (isset($order_limit['group_by']) ? ' '.$this->group_by($order_limit['group_by']) : '');
    }

    /**
     * Get the ORDER BY and LIMIT clauses
     *
     * @param int    $rows
     * @param int    $page
     * @param string $order
     * @param string $key
     * @return string
     */
    abstract protected function order_limit_sensitive($rows, $page, $order, $key);

    /**
     * GROUP BY clause
     *
     * @param string $col_name
     * @return string
     */
    abstract protected function group_by($col_name);
}
