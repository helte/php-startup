<?
namespace Helte\StartUp;

use Helte\StartUp\Concepts\Real;

class Directory extends DiskObject
{
    /** @inheritdoc */
    public function size()
    {
        $size = new Real(0.0);
        $this->eachFile(function(File $file, Directory $parent)use($size){
            $size->increase($file->size());
        }, [], true);
        return $size;
    }

    /**
     * Run the given callback function to each disk object
     *
     * @param callable   $onEach    fn(File|Directory $diskObject, Directory $parent[, ...your args...])
     * @param array      $args
     * @param bool|false $recursive
     * @return bool
     */
    public function each(callable $onEach, array $args=[], $recursive=false)
    {
        $items = array_diff(scandir($this->getPath()), ['.','..']);
        foreach($items as $_item){
            $path = $this->compose($_item);
            $do = null;
            if(is_dir($path)){
                $do = new self($path);
                if($recursive && $do->each($onEach, $args, true) === false){
                    return false;
                }
            }else{
                $do = new File($path);
            }
            if(call_user_func_array($onEach, array_merge([$do, $this], $args)) === false){
                return false;
            }
        }
        return true;
    }

    /**
     * Find and run the given callback function to each file
     *
     * @param callable   $onEach    fn(File $diskObject, Directory $parent[, ...your args...])
     * @param array      $args
     * @param bool|false $recursive
     * @return bool
     */
    public function eachFile(callable $onEach, array $args=[], $recursive=false)
    {
        return $this->each(function(DiskObject $diskObject, Directory $me)use($onEach, $args){
            if($diskObject instanceof File) return call_user_func_array($onEach, array_merge([$diskObject, $me], $args));
            return true;
        }, [], $recursive);
    }

    /**
     * Find and run the given callback function to each sub directory
     *
     * @param callable   $onEach    fn(Directory $diskObject, Directory $parent[, ...your args...])
     * @param array      $args
     * @param bool|false $recursive
     * @return bool
     */
    public function eachDirectory(callable $onEach, array $args=[], $recursive=false)
    {
        return $this->each(function(DiskObject $diskObject, Directory $me)use($onEach, $args){
            if($diskObject instanceof Directory) return call_user_func_array($onEach, array_merge([$diskObject, $me], $args));
            return true;
        }, [], $recursive);
    }

    /**
     * Compose a path with the given relative name and this directory path
     *
     * NOTE: not confirmed if it really exists yet
     * NOTE: not considers which it's a file or directory, just in a string
     *
     * @param string|Text $relative_name
     * @return string
     */
    public function compose($relative_name)
    {
        return $this->getPath() . DIRECTORY_SEPARATOR . $relative_name;
    }
}