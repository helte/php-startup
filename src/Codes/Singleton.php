<?php
namespace Helte\StartUp\Codes;

/**
 * Keep the instance single for a class
 *
 * To make the class singleton, use this trait for exactly THE class
 */
trait Singleton
{
    /**
     * List of instances which once have been instanced
     *
     * @var \Object
     */
    private static $instance;

    /**
     * Constructor should be hidden from public access
     */
    protected function __construct()
    {
    }

    /**
     * This method is the only way to get the instance of the class
     *
     * @return Object
     */
    final public static function getInstance()
    {
        $class = get_called_class();
        if (!isset(self::$instance)) self::$instance = new $class;

        return self::$instance;
    }

    /**
     * Throws an exception when the class is going to create another instance
     *
     * @throws \Exception
     */
    public final function __clone()
    {
        throw new \Exception('Clone is not allowed against' . get_class($this));
    }
}
