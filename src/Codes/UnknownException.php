<?php
namespace Helte\StartUp\Codes;

/**
 * Exception to throw the error is not understandable and need to think
 */
class UnknownException extends  \Exception
{
    /** @var int */
    public static $errorCode = -1;
    
    public function __construct($message, \Exception $e=null)
    {
        parent::__construct($message, self::$errorCode, $e);
    }
}
