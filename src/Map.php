<?php
namespace Helte\StartUp;

/**
 * Associative array as object
 */
class Map extends Arrayal
{
    /**
     * Convert an arrayal to a map
     *
     * @param Arrayal $arrayal
     * @return Map
     */
    public static function fromArrayal(Arrayal $arrayal)
    {
        return new self($arrayal->toArray());
    }

    /**
     * Filter by key array and get another object of map
     *
     * @param mixed[] $keys
     * @param bool    $sensitive
     * @return $this
     */
    public function filterKeyByArray(array $keys, $sensitive=false)
    {
        $cls = get_class($this);
        if(defined('ARRAY_FILTER_USE_KEY')) {
            $map = array_filter($this->arr, function($key)use($keys){
                return in_array($key, $keys);
            }, ARRAY_FILTER_USE_KEY);
        }else{
            $map = [];
            foreach($this->arr as $key=>$value){
                if(in_array($key, $keys)) $map[$key] = $value;
            }
        }
        return !$sensitive || count($map) === count($keys) ? new $cls($map) : false;
    }
}
