<?php
namespace Helte\StartUp\AutoLoaders;


abstract class ClassFileFinder
{
    /**
     * List of instantiated classes of this
     *
     * @var ClassFileFinder[]
     */
    private static $all_finders = [];

    /**
     * @param string   $class_name
     * @param callable $on_hit
     * @param mixed[]  $args
     * @return bool Found or not
     */
    final public static function findInAll($class_name, callable $on_hit, array $args=[])
    {
        foreach(self::$all_finders as $finder){
            if($finder->find($class_name, $on_hit, $args)) return true;
        }
        return false;
    }

    public function __construct()
    {
        self::$all_finders[] = $this;
        spl_autoload_register([$this, 'load']);
    }

    /**
     * @param string   $class_name
     * @param callable $on_hit
     * @param mixed[]  $args
     * @return bool Found or not
     */
    abstract public function find($class_name, callable $on_hit, array $args=[]);

    /**
     * Load the class from the class full name
     *
     * @param string $class_name Class name with all parent namespaces
     * @return bool Whether if the class is loaded
     */
    final public function load($class_name)
    {
        return $this->find($class_name, function($_, $_, $file){
            require_once $file;
        });
    }
}
