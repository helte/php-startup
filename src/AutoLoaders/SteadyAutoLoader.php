<?php
namespace Helte\StartUp\AutoLoaders;

/**
 * # AutoLoader for quick use
 */
class SteadyAutoLoader extends ClassFileFinder
{
    /**
     * @var string
     */
    private $base_dir;

    public function __construct($base_dir)
    {
        $this->base_dir = $base_dir;
        parent::__construct();
    }

    /**
     * Get the set base directory path
     *
     * @return string
     */
    public function getBaseDir()
    {
        return $this->base_dir;
    }

    /**
     * Get the class definition file path from the class full name
     *
     * @param string $class_name Class name with all parent namespaces
     * @return string|bool File path (not confirmed its existence)
     */
    public function getClassFilePath($class_name)
    {
        return $this->base_dir . DIRECTORY_SEPARATOR . strtr($class_name, '\\', DIRECTORY_SEPARATOR) . '.php';
    }

    /**
     * @param string $class_name
     * @param callable $on_hit
     * @param mixed[] $args
     * @return bool Found or not
     */
    public function find($class_name, callable $on_hit, array $args = [])
    {
        $file = $this->getClassFilePath($class_name);
        if($file && is_readable($file)){
            $args1 = array_merge([$this, 0, $file], $args);
            return call_user_func_array($on_hit, $args1) !== false;
        }
        return false;
    }
}
