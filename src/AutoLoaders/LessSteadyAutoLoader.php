<?php
namespace Helte\StartUp\AutoLoaders;

/**
 * # AutoLoader for quick use with little laziness
 *
 * ## Feature #1
 * With a prefix of the namespace to check.
 *
 * ## Feature #2
 * With an inner directory to separate from documents
 */
class LessSteadyAutoLoader extends SteadyAutoLoader
{
    /**
     * @var null|string
     */
    private $prefix;
    /**
     * @var int
     */
    private $prefix_length;

    /**
     * @param string      $base_dir
     * @param null|string $prefix
     */
    public function __construct($base_dir, $prefix=null)
    {
        parent::__construct($base_dir);
        $this->prefix = $prefix;
        $this->prefix_length = is_string($prefix) ? strlen($this->prefix) : -1;
    }

    /**
     * Get the namespace prefix in string
     *
     * @return null|string
     */
    final public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * Get the namespace prefix's length
     *
     * @return int
     */
    final public function getPrefixLength()
    {
        return $this->prefix_length;
    }

    /**
     * Get the class definition file path from the class full name
     *
     * @param string $class_name Class name with all parent namespaces
     * @return string|bool File path (not confirmed its existence)
     */
    public function getClassFilePath($class_name)
    {
        $rel_class_name = $class_name;
        if(is_string($this->getPrefix()) && !empty($this->getPrefix())){
            if(strpos($class_name, $this->getPrefix()) !== 0) return false;
            $rel_class_name = substr($class_name, $this->getPrefixLength()+1);
        }

        $params = explode('\\', $rel_class_name, 2);
        if(!isset($params[1])) return false;

        list($name, $path) = $params;
        $file = $this->getBaseDir() . DIRECTORY_SEPARATOR .
                $name . DIRECTORY_SEPARATOR .
                'src' . DIRECTORY_SEPARATOR .
                strtr($path, '\\', DIRECTORY_SEPARATOR) . '.php';
        return $file;
    }
}
