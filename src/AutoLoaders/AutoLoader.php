<?php
namespace Helte\StartUp\AutoLoaders;


class SimpleAutoLoader
{
    /**
     * @var string
     */
    private $base_dir;

    /**
     * @param string $base_dir
     */
    public function __construct($base_dir)
    {
        $this->base_dir = $base_dir;
    }

    /**
     * Try to load the class definition file, or return false
     *
     * @param string $class_name Full path of the class
     * @return bool
     */
    public function load($class_name)
    {
        $a = explode('\\', $class_name);
        $vendor = array_shift($a);
        $class  = array_pop($a);
        $inner  = implode('/', $a);
        $file   = implode('/', [$this->base_dir, $vendor, 'classes', $inner, $class]) . '.php';
        if(is_readable($file)){
            require_once $file;
            return true;
        }
        return false;
    }
}
