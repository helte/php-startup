<?php
namespace Helte\StartUp\Concepts;


class Decimal extends NativeValue
{
    private $value;
    
    public function __construct($value)
    {
        $this->value = $value;
    }
    
    public function increase($amount=1)
    {
        $this->value += $amount;
    }
    
    public function decrease($amount=1)
    {
        $this->value -= $amount;
    }
    
    public function getValue(){ return $this->value; }

}
