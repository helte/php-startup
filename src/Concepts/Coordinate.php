<?php
namespace Helte\StartUp\Concepts;

class Coordinate
{
    /**
     * Axis X
     *
     * @var int
     */
    public $x;
    /**
     * Axis Y
     *
     * @var int
     */
    public $y;

    public function __construct($x, $y)
    {
        $this->x = $x;
        $this->y = $y;
    }

    /**
     * Value of the distance between 2 coordinates
     *
     * @param Coordinate $another
     * @return int
     */
    public function distance(Coordinate $another)
    {
        return (int)log(pow(abs($this->x - $another->x), 2) + pow(abs($this->y - $another->y), 2), 2);
    }

    /**
     * Create a new instance of integer axises
     *
     * @return Coordinate
     */
    public function integerise()
    {
        return new self((int)$this->x, (int)$this->y);
    }
}
