<?php
namespace Helte\StartUp\Concepts;


class Rect
{
    /** @var int */
    public $width;
    /** @var int */
    public $height;

    /**
     * @param int $width
     * @param int $height
     */
    public function __construct($width, $height)
    {
        if(!is_int($width)){
            throw new \InvalidArgumentException(sprintf(
                '%s::%s requires int for %d%s argument, but %s was given',
                __CLASS__, __METHOD__, 1, 'st', gettype($width)));
        }else if(!is_int($height)){
            throw new \InvalidArgumentException(sprintf(
                '%s::%s requires int for %d%s argument, but %s was given',
                __CLASS__, __METHOD__, 2, 'nd', gettype($height)));
        }else{
            $this->width  = $width;
            $this->height = $height;
        }
    }

    /**
     * Calculate the height by the ratio of the given sample rectangle
     * and apply the value.
     *
     * @param Rect $sample
     * @return $this
     */
    public function setSameRatioForHeight(Rect $sample)
    {
        $this->height = (int)(($sample->height/$sample->width)*$this->width);
        return $this;
    }


    /**
     * Calculate the width by the ratio of the given sample rectangle
     * and apply the value.
     *
     * @param Rect $sample
     * @return $this
     */
    public function setSameRatioForWidth(Rect $sample)
    {
        $this->width = (int)(($sample->width/$sample->height)*$this->height);
        return $this;
    }

    /**
     * Check if this rectangle includes the given one.
     *
     * @param Rect $included
     * @return bool
     */
    public function includable(Rect $included)
    {
        return $this->width > $included->width && $this->height > $included->height;
    }

    /**
     * Get the center of the gravity
     *
     * @return Coordinate
     */
    public function gravity()
    {
        return new Coordinate($this->width / 2, $this->height / 2);
    }

    /**
     * Get the offset coordinate
     *
     * @param Rect $another
     * @param string $gravity Place where the gravity is supposed
     * @return Coordinate
     */
    public function getOffset(Rect $another, $gravity='center')
    {
        return new Coordinate(
            abs(($this->width  - $another->width)/2),
            abs(($this->height - $another->height)/2));
    }

    /**
     * Check if the rectangle height is longer than width
     * like a building.
     *
     * @return bool
     */
    public function vertical(){ return $this->width < $this->height; }

    /**
     * Check if the rectangle width is longer than height
     * like a laptop computer.
     *
     * @return bool
     */
    public function horizontal(){ return $this->width > $this->height; }

    /**
     * Check if the rectangle is a square
     * like a cube.
     *
     * @return bool
     */
    public function square(){ return $this->width === $this->height; }


    /**
     * Calculate the distance of each of width and height
     *
     * NOTE: the result is an absolute value. Not concern which is larger.
     *
     * @param Rect $another
     * @return Rect
     */
    public function diff(Rect $another)
    {
        return new Rect(
            abs($another->width  - $this->width),
            abs($another->height - $this->height));
    }

    /**
     * Get the ratio of width - height
     *
     * @return float
     */
    public function ratio(){ return $this->width / $this->height; }

    /**
     * Check if the rectangle is real size ( the square value is more than 0 ).
     *
     * @return bool
     */
    public function real(){ return $this->width * $this->height > 0; }
}
