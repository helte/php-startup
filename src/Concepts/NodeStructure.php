<?php
namespace Helte\StartUp\Concepts;

class NodeStructure implements \Countable
{
    /**
     * @var NodeStructure|null
     */
    private $parent_structure;

    /**
     * @var NodeStructure[]
     */
    private $children;

    /**
     * ID uniquely associated with the data
     *
     * @var string|int
     */
    private $id;

    /**
     * Referenced data
     *
     * @var mixed
     */
    private $data;

    public function __construct($id, &$data)
    {
        $this->id = $id;
        $this->data = $data;
        $this->children = [];
    }

    /**
     * Get the node ID
     *
     * @return string|int|null
     */
    final public function getId(){ return $this->id; }

    /**
     * @return mixed
     */
    final public function getData(){ return $this->data; }

    /**
     * Get the parent structure or null
     *
     * @return null|NodeStructure
     */
    final public function getParent()
    {
        return $this->parent_structure;
    }

    /**
     * Count of the children structures
     *
     * @param bool $recursive Whether the count number should contain descendants
     * @return int
     */
    final public function count($recursive=false)
    {
        $i = 0;
        if($recursive){
            foreach($this->children as $_child) $i += $_child->count($recursive);
        }
        return $i + \count($this->children);
    }

    /**
     * Add a child
     *
     * @param NodeStructure $child
     */
    final public function add(NodeStructure $child)
    {
        $this->children[] = $child;
    }

    /**
     * Check if a structure is a descendant of this structure.
     * Returns the parent structure if it's in that.
     *
     * @param string|int $id
     * @param bool       $recursive
     * @return $this|bool|NodeStructure
     */
    final public function in($id, $recursive=false)
    {
        foreach($this->children as $_child){
            if($_child->id === $id) return $_child;
            if($recursive){
                $_desc = $_child->in($id, $recursive);
                if ($_desc) return $_desc;
            }
        }
        return false;
    }
}
