<?php
namespace Helte\StartUp\Concepts;

/**
 * Class for float value
 */
class Real extends NativeValue
{
    /** @var float */
    private $value;

    /**
     * @param float $value
     */
    public function __construct($value)
    {
        $this->value = (float)$value;
    }

    /**
     * @param float $amount
     */
    public function increase($amount=1.0)
    {
        $this->value += (float)$amount;
    }

    /**
     * @param float $amount
     */
    public function decrease($amount=1.0)
    {
        $this->value -= (float)$amount;
    }

    /**
     * @return float
     */
    public function getValue(){ return $this->value; }
}
