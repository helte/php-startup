<?php
namespace Helte\StartUp\Concepts;

/**
 * Object to switch 2 things which are against each other
 */
class Dual
{
    /**
     * 1 is positive, 2 is negative, 0 is not chosen
     * 
     * @var int
     */
    private $value;

    /**
     * @param int $value 1 is positive, 2 is negative, 0 is not chosen
     */
    public function __construct($value=0)
    {
        $this->value = $value < 0 ? 0 : ($value > 2 ? 2 : $value);
    }

    /**
     * Set the value suspended, not to choose which
     *
     * @return $this
     */
    final public function suspend()
    {
        $this->value = 0;
        return $this;
    }
    
    /**
     * Check if the value is not chosen yet, or already have been chosen
     *
     * @return bool
     */
    public function isSuspended()
    {
        return $this->value === 0;
    }

    /**
     * Choose which one in positive and negative
     *
     * @param bool $is_positive True to choose positive, false to choose negative
     */
    final protected function choose($is_positive)
    {
        $this->value = $is_positive ? 1 : 2;
    }

    /**
     * Check if the value is positive
     *
     * NOTE: it returns false also if the value is suspended
     * 
     * @return bool
     */
    final protected function isPositive()
    {
        return $this->value === 1;
    }

    /**
     * Check if the value is negative
     *
     * NOTE: it returns false also if the value is suspended
     *
     * @return bool
     */
    final protected function isNegative()
    {
        return $this->value === 2;
    }
}
