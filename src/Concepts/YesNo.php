<?php
namespace Helte\StartUp\Concepts;


class YesNo extends Dual
{
    /**
     * Set as Yes
     *
     * @return $this
     */
    final public function setYes()
    {
        $this->choose(true);
        return $this;
    }

    /**
     * Set as No
     *
     * @return $this
     */
    final public function setNo()
    {
        $this->choose(false);
        return $this;
    }

    /**
     * Check if the value is Yes
     *
     * @return bool
     */
    final public function isYes()
    {
        return $this->isPositive();
    }

    /**
     * Check if the value is No
     *
     * @return bool
     */
    final public function isNo()
    {
        return $this->isNegative();
    }
}
