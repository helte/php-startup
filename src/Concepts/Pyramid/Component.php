<?php
/**
 *
 */
namespace Helte\StartUp\Concepts\Pyramid;

/**
 * One of a pyramid component
 */
class Component
{
    /** @var string */
    private $title;
    /** @var float|int */
    private $gross;
    /** @var Component */
    private $parent;
    /** @var mixed[] */
    private $data;

    /**
     * @param int|float $value Gross value of the component
     * @param Component|null $parent Give the parent component if there is
     *                               Note: the parent must be larger than this component's gross
     * @throws \Exception
     */
    public function __construct($gross, Component $parent=null)
    {
        $this->gross = $gross;
        $this->title = (string)$gross;
        $this->parent = $parent;
        $this->data = [];
    }

    /**
     * Set the component title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get the component title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Update the gross value
     *
     * @param int|float $gross
     * @return $this
     */
    public function setGross($gross)
    {
        $this->gross = $gross;
        return $this;
    }

    /**
     * Update the net value
     *
     * @param int|float $net
     * @return $this
     */
    public function setNet($net)
    {
        $this->gross = is_null($this->parent) ? $net : $this->parent->getGross() - $net;
        return $this;
    }

    /**
     * Get the net (not duplicated) number in any parent composition
     *
     * @return int|float
     */
    public function getNet()
    {
        return is_null($this->parent) ? $this->gross : $this->parent->getGross() - $this->gross;
    }

    /**
     * Get the real value (gross value) of this composition
     *
     * @return int|float
     */
    public function getGross()
    {
        return $this->gross;
    }

    /**
     * Get the given depth ancestor
     *
     * @param int $index
     * @return bool|Component False when it's not found
     */
    public function getAncestor($index=1)
    {
        $i=0;
        $ancestor = $this;
        do{
            if($i === $index) return $ancestor;
            $ancestor = $ancestor->parent;
        }while(!is_null($ancestor));
        return false;
    }

    /**
     * Get the terminal component (largest component)
     *
     * @return Component|null
     */
    public function getTerminal()
    {
        $t = $this;
        while(!is_null($t->parent)){
            $t = $this->parent;
        }
        return $t;
    }

    /**
     * Create a child component into the pyramid
     *
     * @param int|float $child_gross
     * @param null|string|int|float|double $child_title
     * @return $this
     */
    public function child($child_gross, $child_title=null)
    {
        $c = get_class($this);
        $a = new $c($child_gross, $this);
        if(is_scalar($child_title)) $a->setTitle($child_title);
        return $a;
    }

    /**
     * Set a value with a key to the component
     *
     * @param string $name
     * @param mixed $value
     * @return $this
     */
    public function setData($name, $value)
    {
        $this->data[$name] = $value;
        return $this;
    }

    /**
     * Get the value with the given key otherwise gives the default value
     *
     * @param string     $name
     * @param null|mixed $default_value
     * @return mixed
     */
    public function getData($name, $default_value=null)
    {
        return isset($this->data[$name]) ? $this->data[$name] : $default_value;
    }

    /**
     * Clear all data in the component
     *
     * @return $this
     */
    public function clearData()
    {
        $this->data = [];
        return $this;
    }
}
