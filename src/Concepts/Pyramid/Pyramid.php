<?php
/**
 *
 */
namespace Helte\StartUp\Concepts\Pyramid;

class Pyramid
{
    /** @var  Component */
    private $terminal;

    /**
     * @param Component $terminal Set the terminal component which is the largest in the pyramid
     */
    public function __construct(Component $terminal)
    {
        $this->terminal = $terminal;
    }
}
