<?php
namespace Helte\StartUp\Concepts;

use Helte\StartUp\Objective;

/**
 * Common base class for Decimal and Real
 */
abstract class NativeValue extends Objective
{
    abstract public function increase($amount=1);
    abstract public function decrease($amount=1);
    abstract public function getValue();

    /**
     * @inheritdoc
     *
     * NOTE: this method compares the internal value, nor the surface,
     * which means it also passes if you give it integer.
     */
    public function eq($another){
        return is_object($another) && $another instanceof self ? $this->value === $another->value : (
        is_int($another) ? $this->value === $another : false
        );
    }
}