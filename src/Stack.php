<?php
namespace Helte\StartUp;

/**
 * Array which is not associative (= list)
 */
class Stack extends Arrayal
{
    /**
     * Add an element at the first of the stack
     *
     * @param mixed $element
     * @return $this
     */
    public function prepend($element)
    {
        array_unshift($this->arr, $element);
        return $this;
    }

    /**
     * Add an element at the end of the stack
     *
     * @param mixed $element
     * @return $this
     */
    public function append($element)
    {
        $this->arr[] = $element;
        return $this;
    }
}
