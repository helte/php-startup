<?php
namespace Helte\StartUp;


interface ArrayFilter
{
    /**
     * Do something with affection on the given array
     * 
     * @param array $arr
     */
    public function filter(array &$arr);
}
