<?php
namespace Helte\StartUp\RSS2;


class RSS2
{
    /**
     * Write a RSS document at all
     *
     * @param string $site_title
     * @param string $site_rss_url
     * @param string $site_description
     * @param string $site_language
     * @param string $site_author
     * @param array $items
     * @param callable $argument_converter
     * @return void
     */
    public static function display($site_title, $site_rss_url, $site_description, $site_language, $site_author, array $items, callable $argument_converter)
    {
        self::send_headers();
        self::write_header();
        self::write_rss($site_title, $site_rss_url, $site_description, $site_language, $site_author, $items, $argument_converter);
    }

    /**
     * Send HTTP headers
     * @return void
     */
    private static function send_headers()
    {
        header('Content-Type: application/rss+xml; charset=utf-8');
    }

    /**
     * Write RSS document header
     * @return void
     */
    private static function write_header()
    {
        echo '<?xml version="1.0" encoding="UTF-8"?>', "\n";
    }

    /**
     * Write a RSS document at all
     *
     * @param string $site_title
     * @param string $site_rss_url
     * @param string $site_description
     * @param string $site_language
     * @param string $site_author
     * @param array $items
     * @param callable $argument_converter
     * @return void
     */
    private static function write_rss($site_title, $site_rss_url, $site_description, $site_language, $site_author, array $items, callable $argument_converter)
    {
        echo '<rss version="2.0" ', "\n";
	    echo "\t", 'xmlns:dc="http://purl.org/dc/elements/1.1/"', "\n";
	    echo "\t", 'xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"', "\n";
	    echo "\t", 'xmlns:admin="http://webns.net/mvcb/"', "\n";
	    echo "\t", 'xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">', "\n";

        echo "\t\t<channel>\n";
        self::write_channel($site_title, $site_rss_url, $site_description, $site_language, $site_author, $items, $argument_converter);
        echo "\t\t</channel>\n";

        echo "</rss>\n";
    }

    /**
     * Write a &lt;channel&gt; content
     *
     * @param string $site_title
     * @param string $site_rss_url
     * @param string $site_description
     * @param string $site_language
     * @param string $site_author
     * @param array $items
     * @param callable $argument_converter
     * @return void
     */
    private static function write_channel($site_title, $site_rss_url, $site_description, $site_language, $site_author, array $items, callable $argument_converter)
    {
        $site_rss_last_update = date('c');
        echo "\t\t\t<title>${site_title}</title>\n";
		echo "\t\t\t<link>${site_rss_url}</link>\n";
		echo "\t\t\t<description>${site_description}</description>\n";
		echo "\t\t\t<dc:language>${site_language}</dc:language>\n";
		echo "\t\t\t<dc:creator>${site_author}</dc:creator>\n";
		echo "\t\t\t<dc:date>${site_rss_last_update}</dc:date>\n";

        foreach($items as $item){
            $args = call_user_func($argument_converter, $item);
            if(count($args) > 5){
                self::write_item($args[0], $args[1], $args[2], $args[3], $args[4], $args[5]);
            }
        }
    }

    /**
     * Write a single &lt;item&gt; content
     *
     * @param string $title
     * @param string $url
     * @param string $description
     * @param string $category
     * @param string $author
     * @param string $date
     * @return void
     */
    private static function write_item($title, $url, $description, $category, $author, $date)
    {
        $date = date('c', strtotime($date));
        echo "\t\t\t<item>\n";
        echo "\t\t\t\t<title>$title</title>\n";
        echo "\t\t\t\t<link>$url</link>\n";
		echo "\t\t\t\t<description><![CDATA[$description]]></description>\n";
        echo "\t\t\t\t<dc:subject>$category</dc:subject>\n";
        echo "\t\t\t\t<dc:creator>$author</dc:creator>\n";
		echo "\t\t\t\t<dc:date>$date</dc:date>\n";
        echo "\t\t\t</item>\n";
    }
}
