<?php
use Helte\StartUp\RSS2\RSS2;
require_once 'RSS2.php';

RSS2::display('Test', 'http://example.com/test.rss', 'This rss is test', 'en', 'Masato Ohta', [
    [
        'Test article',
        'http://exmaple.com/articles/test.html',
        'This article is a test',
        'test',
        'Masato Ohta',
        date('Y/m/d H:i:s')
    ]
], function(array $item){
    return $item;
});
