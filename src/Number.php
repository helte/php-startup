<?php
namespace Helte\StartUp;


class Number
{
    /**
     * Convert the value into integer
     *
     * @param int|float|double $value
     * @return int
     */
    public static function toInt($value)
    {
        return (int)$value;
    }
    /**
     * Check if the number is in the range of the given numbers
     *
     * @param int|float|double $value
     * @param int|float|double $min
     * @param int|float|double $max
     * @return bool
     */
    public static function between($value, $min, $max)
    {
        return $min <= $value && $value <= $max;
    }

    /**
     * Check if the number is less than the given number
     *
     * @param int|float|double $value
     * @param int|float|double $more
     * @return bool
     */
    public static function lessThan($value, $more)
    {
        return $value < $more;
    }

    /**
     * Check if the number is more than the given number
     *
     * @param int|float|double $value
     * @param int|float|double $less
     * @return bool
     */
    public static function moreThan($value, $less)
    {
        return $value > $less;
    }

    /**
     * Check if the number is equivalent to the given number
     *
     * @param int|float|double $value
     * @param int|float|double $eq
     * @return bool
     */
    public static function eq($value, $eq)
    {
        return $value == $eq;
    }
}
