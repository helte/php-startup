<?php
namespace Helte\StartUp;

use Helte\StartUp\Concepts\Real;

/**
 * Base class for a disk object (local disk IO)
 *
 * NOTE: in the meaning of DiskObject, remote disk object is out of this class.
 * This class only means a local disk object.
 */
abstract class DiskObject extends Objective
{
    /**
     * Disk object path
     * 
     * @var string
     */
    private $path;

    /**
     * @param string $path Disk object path
     */
    public function __construct($path){ $this->path = $path; }

    /**
     * @see File::__toString()
     * @return string
     */
	public function getPath(){ return $this->__toString(); }
    
    /**
     * It automatically casts into generic string
     * @return string
     */
    final public function __toString(){ return $this->path; }

    /**
     * Confirm the existence of the file
     *
     * @return bool
     */
    public function exists(){ return \file_exists($this->path); }

    /**
     * Confirm the permission to read
     *
     * @return bool
     */
    public function readable(){ return \is_readable($this->path); }

    /**
     * Confirm the permission to write
     * 
     * @return bool
     */
    public function writable(){ return \is_writable($this->path); }

    /**
     * Delete the disk object
     *
     * @return bool
     */
    public function delete(){ return \unlink($this->path); }

    /**
     * Get the size of the disk object
     *
     * @return Real
     */
    abstract public function size();

    /**
     * Move the disk object to another destination
     *
     * @param DiskObject $destination
     * @param bool|false $overwrite
     * @param bool|false $uploaded
     * @return bool
     */
    public function move(DiskObject $destination, $overwrite=false, $uploaded=false)
    {
        if($destination->exists()){
            if($overwrite) \unlink($destination->path);
            else { return false; }
        }
        if($uploaded){
            return \move_uploaded_file($this->path, $destination->path);
        }else{
            return \rename($this->path, $destination->path);
        }
    }
}
