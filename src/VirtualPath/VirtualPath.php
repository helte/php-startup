<?php
namespace Helte\StartUp\VirtualPath;

use Helte\StartUp\Map;
use Helte\StartUp\Text;

/**
 * Variably operate URL with PHP thrown from .htaccess
 *
 * As moving your web application from a server to another server, the URL might be
 * different. Even in this case, this class makes sure your web application keeps
 * running because of variable management.
 */
class VirtualPath
{
    /**
     * @var Text
     * @see getPath()
     */
    private $path;
    /**
     * @var Text
     * @see getRequestRoot()
     */
    private $request_root;
    /**
     * @var Text
     * @see getRequestPath()
     */
    private $request_path;
    /**
     * @var Text
     * @see getIndex()
     */
    private $index;

    /**
     * @param Text|null $abs_url
     * @param Text|null $abs_index
     */
    public function __construct(Text $abs_url=null, Text $abs_index=null)
    {
        $server = new Map($_SERVER);
        if(is_null($abs_url)){
            $abs_url   = new Text($server->get('REQUEST_URI', '/'));
        }
        if(is_null($abs_index)){
            $abs_index = new Text($server->get('SCRIPT_NAME', '/index.php'));
        }

        $this->request_root = new Text(dirname($abs_index));
        $this->request_path = $abs_url->before('?');
        if($this->request_path->has('%')) $this->request_path = new Text(urldecode($this->request_path));
        $this->index = $abs_index;

        $this->path = $this->request_path->sub($this->index->sub(0, strlen(basename(__FILE__)) * -1)->length());

        if(!$this->path->eq('/') && $this->path->sub(-1)->eq('/')){
            $this->path->cut(0, -1);
        }
    }

    /**
     * Get the request root path in URL
     *
     * The web root path, which indicates the top page content.
     * This path is always a directory expression, not a file.
     *
     * You should make sure it's always URL, not physical path on
     * the server.
     *
     * @return Text
     */
    public function getRequestRoot(){ return $this->request_root; }

    /**
     * Get the virtual path which starts with "/" (no "/" at the end even for directory)
     *
     * This path is always absolute, not relative to any path.
     * It means the path always starts with "/" and
     * at the end path does NOT have "/" even for directory.
     *
     * @return Text
     */
    public function getPath(){ return $this->path; }

    /**
     * Get the request path in URL
     *
     * The path which is displayed on the accessor's web browser
     * without "?" and "#", nor any parameters.
     *
     * You should make sure it's always URL, not physical path on
     * the server.
     *
     * @return Text
     */
    public function getRequestPath(){ return $this->request_path; }

    /**
     * Get the entry point for the variable path
     *
     * The physical file which is the entry point of the current process.
     * It will be often "index.php" instead of non existing file.
     *
     * @return Text
     */
    public function getIndex(){ return $this->index; }
}
