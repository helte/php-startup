<?php
namespace Helte\StartUp\Theme;

use Helte\StartUp\VirtualPath\VirtualPath;

class Theme
{
    /**
     * Name of the selected theme
     *
     * @var string
     */
    private $name;
    /**
     * Destination of the content: name in "contents" in a theme
     *
     * @var string
     */
    private $content_destination;
    /**
     * Meta data for the website
     *
     * @var array
     */
    private $meta;
    /**
     * Flag whether Theme::display() is called or not yet
     *
     * @var bool
     */
    private $displayed;

    /**
     * Set true when it's running on debug mode
     *
     * @var bool
     */
    public $onDebug = false;

    /**
     * Path to "themes" directory
     *
     * @var string
     */
    private $theme_dir;

    /**
     * @var VirtualPath
     */
    private $path;

    /**
     * @param string $name Name of the selected theme
     * @param string $theme_dir Path to "themes" directory
     * @param array  $meta
     */
    public function __construct($name, $theme_dir, VirtualPath $path, array $meta=[])
    {
        $this->name = $name;
        $this->meta = $meta;
        $this->displayed = false;
        $this->content_destination = 'error';
        $this->debug_values = [];
        $this->theme_dir = $theme_dir;
        $this->path = $path;
    }

    /**
     * Get the virtual path object
     *
     * @return VirtualPath
     */
    public function getVirtualPath()
    {
        return $this->path;
    }

    /**
     * Show the whole page
     *
     * @param array $args
     * @return $this
     */
    final public function display(array $args=[])
    {
        if(!$this->displayed) $this->section('main', $args);
        return $this;
    }

    /**
     * Display a section only
     *
     * @param string $name Name of the section (the same as the file name for the section without extension)
     * @param array  $args
     * @return $this
     */
    final public function section($name, array $args=[])
    {
        if($name === 'content'){
            $name = 'contents/' . $this->content_destination;
        }

        $file = $this->theme_dir . '/' . $this->name . '/' . $name . '.php';
        $this->debug($file);

        if(is_readable($file)){
            $theme = $this;
            require_once $file;
        }
        return $this;
    }

    /**
     * Get the URL with the path
     *
     * @param string $path
     * @return string
     */
    final public function url($path)
    {
        return $this->path->getRequestRoot()->sub(0, -1) . $path;
    }

    /**
     * Get the URL with the path
     *
     * @param string $path
     * @return string
     */
    final public function urlInTheme($path)
    {
        return $this->path->getRequestRoot() . 'themes/' . $this->name .$path;
    }

    /**
     * Set a name in "contents" in a theme as the content
     *
     * @param $content_destination
     * @return $this
     */
    final public function setContentDestination($content_destination)
    {
        $this->content_destination = $content_destination;
        return $this;
    }

    /**
     * Set a new value of the key
     *
     * @param string $key
     * @param mixed $value
     * @return $this
     */
    final public function setValue($key, $value)
    {
        $this->meta[$key] = $value;
        return $this;
    }

    /**
     * Set values at once
     *
     * @param array $meta
     * @return $this
     */
    final public function setValues(array $meta)
    {
        foreach($meta as $key=>$value){
            $this->setValue($key, $value);
        }
        return $this;
    }

    /**
     * Get the value of the given key in the meta data
     *
     * @param string        $key
     * @param null|mixed    $default_value
     * @param null|callable $filter
     * @param null|callable $modifier
     * @return null|mixed
     */
    final public function getValue($key, $default_value=null, callable $filter=null, callable $modifier=null)
    {
        return isset($this->meta[$key]) && (
         !is_callable($filter) || call_user_func($filter, $this->meta[$key])
        ) ? (
         is_callable($modifier) ? call_user_func($modifier, $this->meta[$key]) : $this->meta[$key]
        ) : $default_value;
    }

    /**
     * Get all the meta data values
     *
     * @return array
     */
    final public function getValues()
    {
        return $this->meta;
    }

    /**
     * Get the value in string
     *
     * @param string $key
     * @param string $default_value
     * @return string
     */
    final public function getString($key, $default_value='')
    {
        return $this->getValue($key, $default_value, 'is_string');
    }

    /**
     * Get the value in integer
     *
     * @param string $key
     * @param int    $default_value
     * @return int
     */
    final public function getInt($key, $default_value=0)
    {
        return (int)$this->getValue($key, $default_value, 'is_numeric');
    }

    /**
     * Get the value in integer within the values
     *
     * @param string $key
     * @param int    $min
     * @param int    $max
     * @param int    $default_value
     * @return int
     */
    final public function getIntWithin($key, $min, $max, $default_value=0)
    {
        $value = $this->getInt($key, $default_value);
        return $min > $value ? $min : ($max < $value ? $max : $value);
    }

    /**
     * Call a in-meta callback
     *
     * @param string        $key
     * @param array         $args
     * @param callable|null $default_callback
     * @param bool|mixed    $default_return
     * @return mixed|null|bool
     */
    final public function call($key, array $args=[], callable $default_callback=null, $default_return=false)
    {
        $theme = $this;
        return $this->getValue($key, $default_callback, 'is_callable', function($callback)use($theme, $args, $default_return){
            if(is_callable($callback)){
                return call_user_func_array($callback, array_merge([$theme], $args));
            }else{
                return $default_return;
            }
        });
    }

    /**
     * List of the value of the debug
     *
     * @var mixed[]
     */
    private $debug_values;

    /**
     * Show debug information of the value
     *
     * @param mixed $value
     * @param bool|false $dying
     */
    final public function debug($value, $dying=false)
    {
        if(!$this->onDebug) return;
        $trace = debug_backtrace();
        $this->debug_values[] = [$value, $trace[0]];
        if($dying) die;
    }

    /**
     * Show debug information if something has been saved before the end
     */
    function __destruct()
    {
        foreach($this->debug_values as $data){
            list($value, $trace) = $data;
            echo '<h1>',$trace['file'],'#',$trace['line'],'</h1>';
            echo '<pre>';
            var_dump($value);
            echo '</pre>';
        }
    }


}